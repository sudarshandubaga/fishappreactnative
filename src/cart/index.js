import React, { Component, useState } from "react";
import { Text, TouchableOpacity, View } from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { Icon } from "react-native-elements";
import { styles } from "../screens/WishlistScreen/styles";
import { colors } from "../configs/colors.config";

export default class CartBtn extends Component {
    state = {
        cartItems: {}
    }

    saveCart = async cartItems => {
        
        this.setState({cartItems});
        try {
            await AsyncStorage.setItem('@cart_items', JSON.stringify(cartItems));
        } catch (err) {
            console.log('async error: ', err);
        }
    }

    getCart = async () => {
        let cartItems = await AsyncStorage.getItem('@cart_items');
        cartItems = cartItems !== null ? JSON.parse(cartItems) : {};
        return cartItems;
    }

    addToCart = async (p, id) => {
        let cartItems = await this.getCart();

        p.qty = 1;
        cartItems[id] = p;
        
        this.saveCart(cartItems);
    }

    updateCart = async (id, type) => {
        let cartItems = await this.getCart();

        let qty = cartItems[id].qty;
            qty = parseInt(qty);
    
        if(type === 'plus') {
            qty += 1;
        } else {
            qty -= 1;
        }
    
        if(qty > 0)
            cartItems[id].qty = qty;
        else
            delete cartItems[id]

               
        this.saveCart(cartItems);
    }

    async componentDidMount () {
        // await AsyncStorage.removeItem('@cart_items');
        let cartItems = await this.getCart();
        this.saveCart(cartItems);
    }

    render () {
        let cartItems = this.state.cartItems;
        let {id, product} = this.props;
        return (
            <>
                {
                    cartItems && cartItems[id]
                        ?
                        <View style={{flex:.1, justifyContent: 'flex-end', paddingHorizontal: 10}}>
                            <View style={{ backgroundColor:colors.secondary,borderRadius:50,justifyContent:'center',width:30 }}>
                                <TouchableOpacity onPress={() => this.updateCart(id, 'minus')}>
                                    <View style={styles.add_cart_btn}>
                                    <Icon type="antdesign" color={colors.white} name="minus" />
                                    </View>
                                </TouchableOpacity>
                                <View style={{alignItems:'center',justifyContent:'center',width:30,paddingVertical:0}}>
                                    <Text style={{color:colors.white,fontWeight:'bold',fontSize:17,justifyContent:'center'}}>{ cartItems[id].qty }</Text>
                                </View>
                                <TouchableOpacity onPress={() => this.updateCart(id, 'plus')}>
                                    <View style={styles.add_cart_btn}>
                                    <Icon type="antdesign" color={colors.white} name="plus" />
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                        :
                        <View style={{flex:.1,justifyContent:'flex-end',paddingHorizontal:10}}>
                            <TouchableOpacity onPress={() => this.addToCart(product, id)}>
                                <View style={styles.add_cart_btn}>
                                <Icon type="antdesign" color={colors.white} name="plus" />
                                </View>
                            </TouchableOpacity>
                        </View>
                }
            </>
        );
    }
}