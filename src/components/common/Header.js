import React, { Component } from "react";
import { View, Text, StyleSheet } from "react-native";
import { Icon } from "react-native-elements";
import { colors } from "../../configs/colors.config";

export default class Header extends Component {
    render() {
        let {navigation} = this.props;
        return (
            <>
                <View style={{flex:.7,position:'absolute'}}>
                    <Icon
                        reverse
                        name='chevron-back-outline'
                        type='ionicon'
                        color='#27ae60'
                        style={{ textAlign:"left" }}
                        onPress={() => navigation.goBack()}                            
                    />
                </View>
                <View style={{alignItems:'center'}}>
                    <Text style={styles.navigation_title}>{this.props.route.name}</Text>
                </View>
            </>
        );
    }
}

const styles = StyleSheet.create({
    navigation_title: {
        color:colors.white,
        fontSize: 25,
        marginVertical:18,
        justifyContent:'center',
        
    },
});