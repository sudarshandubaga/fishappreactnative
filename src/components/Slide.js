import React, { Component } from "react";
import { Image, View } from "react-native";

class Slide extends Component {
    render() {
        let { index, styles, image } = this.props;
        return (
            <View style={styles.slide} key={index}>
                <Image style={styles.product_image} source={{uri: image}} />
            </View>
        );
    }
}
export default Slide;