import React, { Component } from "react";
import { View, Image, Text, StyleSheet, TouchableOpacity } from "react-native";
import { Icon } from "react-native-elements/dist/icons/Icon";
import { add_Wishlist, remove_Wishlist, show_Wishlist } from "../Api/wishlist.api";
import CartBtn from "../cart";
import { colors } from "../configs/colors.config";
import { url } from "../configs/constants.config";

export default class ProductBox extends Component {
  state  = {
    addWishListItem: [],
    wishListItem: [],
  }
  addToCart = () => {
    this.props.addToCart(this.props.data, this.props.id)
  }
  updateCart = (type) => {
    if(this.props.cartItems[this.props.id]) {
      this.props.updateCart(this.props.id, type);
    }    
  
  }
  addWishlist = async (id) => {
    let data = {
      product_id :  id
    }
    console.log('data', data);
    let response = await add_Wishlist(data);
    
    if(response.status) {
      this.setState({addWishListItem: response.data});
     }
     let abcd = await getpopularproducts();
    
  }
  removeWishlist = async (favorite_id, id) => {
    let data = {
      favorite_id :  favorite_id,
      product_id  : id
    }
    console.log('data', data);
    let response = await remove_Wishlist(data);
    
    if(response.status) {
      // this.setState({addWishListItem: response.data});
     }
    
  }
  showWishlist = async () => {
    let response = await show_Wishlist();
    
    if(response.status) {
      this.setState({wishListItem: response.data.data});
     }
  }
  async componentDidMount () {
    this.showWishlist();
    
  }
  render () {
    
      let { id, name, path, products_price, favorite, favorite_id } = this.props.data;
      
      return (
          <View style={styles.product_box}>
              <View style={styles.product_box_bg}>
                  <Image style={styles.product_image} source={{uri: url + path} } />
                  <View style={styles.cart_box}>
                    <CartBtn {...this.props} id={id} product={this.props.data} />
                  </View>
              </View>
              <View style={styles.product_title_content}>
                <Text style={styles.product_title}>{name}</Text>
                <Text style={styles.product_price}>{products_price}</Text>
              </View>
              <View style={styles.heart_icon_position}>
                {
                  favorite == 'yes'
                  ?
                  <TouchableOpacity onPress={() => this.removeWishlist(favorite_id, id)}>
                    <View style={{padding:8}}>
                      <Icon                                         
                          name='heart'
                          type='ionicon'
                          iconStyle={{fontSize:22}}
                          color={'red'}
                          
                      />
                    </View>
                  </TouchableOpacity>
                  :
                  <TouchableOpacity onPress={() => this.addWishlist(id)}>
                    <View style={{padding:8}}>
                      <Icon                                         
                          name='heart-outline'
                          type='ionicon'
                          iconStyle={{fontSize:22}}
                          color={'red'}
                          
                      />
                    </View>
                  </TouchableOpacity>
                }
              </View>
          </View>
      );
  }
}

const styles = StyleSheet.create({
  cart_box: {
    position: 'absolute',
    bottom: 5,
    right: 5
  },
  add_cart_btn: {
    backgroundColor: colors.primary,
    // alignItems:'center',
    borderRadius: 15,
    width: 30,
    height: 30,
    justifyContent:'center'
  },
  product_box_bg: {
    position: 'relative',
    backgroundColor: colors.lightBgColor,
    borderRadius: 10,
    alignItems: 'center',
    overflow: 'hidden'
  },
  product_image: {
    width: '100%',
    height: 150,
    resizeMode: 'center',
    
  },
  product_title_content: {
    
  },
  product_title: {
    fontSize:16,
    marginTop:10,
    fontWeight:'bold'   
  },
  product_price: {
    color: '#0d0d0d',
    fontSize: 20,
    fontWeight: 'bold'
  },
  heart_icon_position: {
    position:'absolute',
    top:0,
    right:'5%'
  }
});