import AsyncStorage from "@react-native-async-storage/async-storage";
import React, { Component } from "react";
import { Image, StyleSheet, Text, View, TouchableOpacity } from "react-native";
import { Icon } from "react-native-elements/dist/icons/Icon";
import { colors } from "../configs/colors.config";
// import Svg, { Circle, G, Path, Rect } from "react-native-svg";




export default class Bottomnavigation extends Component {
    state = {
        cartItems:'',
        cart_count:'',
        lastRefresh: Date(Date.now()).toString(),
    }
    async componentDidMount () {
        this.getCart();
        console.log('cart_count',this.state.cart_count);
    }
    
    getCart = async () => {
        let cartItems = await AsyncStorage.getItem('@cart_items');
        cartItems = cartItems !== null ? JSON.parse(cartItems) : {};
        this.setState({cartItems: cartItems});

        this.setState({cart_count: Object.keys(cartItems).length});
        
    }
    refreshScreen() {
        this.setState({ lastRefresh: Date(Date.now()).toString() });
    }

    render() {
        // console.log('home props', this.props.route);
        let {navigation} = this.props;
        let route = this.props.route;
        return (
            <View style={{position:'absolute',bottom:0, left: 0, right: 0, backgroundColor: colors.primary, height: 70, borderTopLeftRadius: 40, borderTopEndRadius: 40}}>
                {/* <Image source={require('../../imgs/logo.png')} /> */}
                {/* <Svg width="100%" height={140} viewBox="0 0 1080 338.77">
                    <G id="Layer_2" data-name="Layer 2">
                        <G id="Layer_1-2" data-name="Layer 1">
                            <Rect x="0" y="250" fill="#27ae60" width='1080' height="338.77" />
                            <Path fill="#27ae60" d="M978.49,93.77l-283.55-13a23.8,23.8,0,0,0-23.56,20.17c-6.23,40.92-31.3,129.83-131.38,129.83-99.68,0-123.56-88.18-129.29-129.32a24,24,0,0,0-23.77-20.68l-285.43,14C45.45,94.77,0,126.22,0,182.28v55A101.52,101.52,0,0,0,101.51,338.77h877A101.52,101.52,0,0,0,1080,237.26v-55C1080,126.22,1034.55,93.77,978.49,93.77Z"/>
                            <Circle fill="#27ae60" cx="540.49" cy="98.38" r="98.38"/>
                            <Circle fill="#fff" cx="540.49" cy="98.38" r="73.79"/>
                        </G>
                    </G>
                </Svg> */}
                <View style={{position:'absolute', left: 0, right: 0,flex: 1, flexDirection: 'row', flexWrap: 'wrap', alignItems: 'center'}}>
                    <View style={styles.btm_menu_content}>
                        <View style={{flex: 1, flexDirection: 'row', flexWrap: 'wrap', alignItems: 'center'}}>
                            <View style={styles.btm_menu_icon_space}>
                                <TouchableOpacity  onPress={()  => this.refreshScreen()}>
                                    <Icon 
                                        name={route.name == 'Home' ? 'home' : 'home-outline'}
                                        color={route.name == 'Home' ? colors.white : colors.bgColor}
                                        type='ionicon'
                                        iconStyle={{fontSize:25}}
                                    />
                                    <Text style={styles.btm_menu_title}>Home</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={styles.btm_menu_icon_space}>
                                <TouchableOpacity onPress={() => navigation.navigate('Order')}>
                                    <Icon
                                        name='receipt-outline'
                                        type='ionicon'
                                        // iconStyle={{fontSize:25}}
                                        color={colors.bgColor}
                                    />
                                    <Text style={styles.btm_menu_title}>Order</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                    <View style={{width:'20%'}}>
                        <View style={{flex: 1, flexDirection: 'row', flexWrap: 'wrap', alignItems: 'center'}}>
                            <View></View>
                        </View>
                    </View>
                    

                    <View style={styles.btm_menu_content}>
                        <View style={{flex: 1, flexDirection: 'row', flexWrap: 'wrap', alignItems: 'center'}}>
                            <View style={styles.btm_menu_icon_space}>
                                <TouchableOpacity onPress={() => navigation.navigate('Wishlist')}>
                                    <Icon 
                                    
                                    name='heart-outline'
                                    type='ionicon'
                                    iconStyle={{fontSize:25}}
                                    color={colors.bgColor} />
                                    <Text style={styles.btm_menu_title}>Wishlist</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={styles.btm_menu_icon_space}>
                                <TouchableOpacity onPress={() => navigation.navigate('Profile')}>
                                    <Icon 
                                    
                                    name='options-outline'
                                    type='ionicon'
                                    color={colors.bgColor} />
                                    <Text style={styles.btm_menu_title}>Menu</Text>
                                </TouchableOpacity>
                                {/* <View style={{borderWidth:3,borderColor: '#bee7cf',borderRadius:50}}>
                                    <Image style={styles.btm_menu_profile_img} source={require('../../imgs/logo.png')} />
                                </View> */}
                                
                            </View>
                        </View>
                    </View>
                </View>
                
                <View
                style={{
                        position: 'absolute',
                        left: '50%',
                        top: -40,
                        width: 80,
                        height: 80,
                        borderBottomColor: colors.white,
                        borderLeftColor: colors.white,
                        borderTopColor: 'transparent',
                        borderRightColor: 'transparent',
                        borderWidth: 5,
                        marginLeft: -40,
                        // backgroundColor: '#fff',
                        borderRadius: 50,
                        transform: [{ rotate: '-45deg'} ]
                        // padding: 3
                    }}>
                        
                    <View style={{
                        width: 70,
                        height: 70,
                        borderColor: colors.primary,
                        backgroundColor: colors.white,
                        borderWidth: 8,
                        borderRadius: 50,
                        justifyContent:'center',
                        
                    }}>
                        
                        <TouchableOpacity onPress={() => navigation.navigate('Cart')}>
                        {
                                    this.state.cart_count > 0 
                                    ?
                                    <View style={{
                                        transform: [{ rotate: '45deg'} ],
                                        position:'absolute',
                                        top:0,
                                        left:'80%',
                                        backgroundColor:'red',
                                        // padding:10,
                                        borderRadius:50,
                                        height:25,
                                        width:25,
                                        justifyContent:'center',
                                        alignContent:'center',
                                        alignItems:'center'

                                    }}>
                                        
                                        <Text style={{
                                            color:colors.white,
                                            fontSize:18,
                                            fontWeight:'bold'
                                        }}>{this.state.cart_count}</Text>
                                    </View>
                                    :
                                    <>
                                    </>

                                }
                        
                            <View style={{transform: [{ rotate: '46deg'} ]}}>
                                <Icon 
                                    
                                    name='cart-outline'
                                    type='ionicon'
                                    color="red"
                                    iconStyle={{fontSize:25}}
                                    />
                            </View>
                        </TouchableOpacity>
                        
                    </View>
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    btm_menu_content: {
        width:'40%',
        marginVertical:16
    },
    btm_menu_icon_space: {
        width:'50%',
        alignItems:'center'
    },
    btm_menu_title: {
        color: colors.bgColor,
    },
    btm_menu_profile_img: {
        width: 30,
        height: 30,
        borderRadius:50,
        
    }
})
