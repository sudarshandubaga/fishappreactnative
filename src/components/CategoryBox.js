import React, { Component } from "react";
import { View, Image, StyleSheet, Text } from "react-native";
import { colors } from "../configs/colors.config";
import { url } from "../configs/constants.config";

export default class CategoryBox extends Component {
    render () {
        let { id, name, path , parent_count} = this.props.data;
        
        return (
            <View>
                
                <View style={styles.cat_box} key={id}>
                    <View style={styles.cat_box_bg}>
                        <Image style={styles.cat_image} source={{uri: url+path}} />
                    </View>
                    
                    <View style={styles.cat_title_content}>
                        <Text style={styles.cat_title} numberOfLines={1}>{name}</Text>
                    </View>                                 
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    
      cat_box_bg: {
        backgroundColor: colors.lightBgColor,
        borderRadius:10,
        overflow:'hidden'
      },
      cat_image: {
        width: '100%',
        height: 100,
        // borderRadius:50,
        // margin:20,
        resizeMode:'center'
      },
      cat_title_content: {
        
      },
      cat_title: {
        textAlign:'center',
        fontSize:16,
        marginVertical:10,
        
        fontWeight:'bold'   
      },
});