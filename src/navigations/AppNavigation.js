import HomeScreen from '../screens/Home';



const MainNavigator =               (
    {
      Home: HomeScreen,
    },
    {
      initialRouteName: 'Home',
      // headerMode: 'float',
      defaulfNavigationOptions: ({ navigation }) => ({
        headerTitleStyle: {
          fontWeight: 'bold',
          textAlign: 'center',
          alignSelf: 'center',
          flex: 1,
        }
      })
    }
  ); 