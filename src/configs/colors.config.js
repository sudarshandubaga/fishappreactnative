export const colors = {
    primary: '#27ae60',
    secondary: '#35bc6e',
    bgColor: '#bee7cf',
    lightBgColor: '#e9f7ef',
    white: '#fff',
    black: '#000',
    grey: '#ccc',
    darkgrey: '#a1a1a1'
}