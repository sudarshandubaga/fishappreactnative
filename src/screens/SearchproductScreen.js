import React, { Component } from 'react';
import { ScrollView, StyleSheet, Text, View, TouchableOpacity, TextInput } from 'react-native';
import { get_main_categories } from '../Api/category.api';
import { getpopularproducts } from '../Api/product.api';
import { colors } from '../configs/colors.config';

export default class SearchproductScreen extends Component {
  state = {
    products: [],
    // searchTerm: '',
    filteredLists: []
  }

  getproducts = async () => {
    let response = await getpopularproducts(); 
      
    if(response.status) {
      this.setState({products: response.data}); 
    } 
  }
  getMainCategories = async () => {
    let response = await get_main_categories();
    
     if(response.status) {
       this.setState({products: response.data});
      }
      
  }

  async componentDidMount () {
     this.getproducts();
     this.getMainCategories();
   }
  searchUpdated(text) {
    var filteredLists;
    if(text && text.length) {
      filteredLists = this.state.products.filter((p) => {
        text = text.toLowerCase();
        return p.name.toLowerCase().search(text) !== -1;
      });
      // console.log('filtered: ', filteredLists);
    } else {
      filteredLists = [];
    }

    this.setState({filteredLists})
  }
  render() {
    
    return (
      
      <View style={{backgroundColor:colors.primary,headerMode: 'screen',height:'100%',}}>
        <View>
          <TextInput
            style={styles.search_input}
            onChangeText={text => this.searchUpdated(text)}
            placeholder="Search fishes..."
            autoFocus
          />
        </View>
        <View style={styles.main_content}>
          <ScrollView style={styles.scrollView}>
            <View style={{padding:10}}>
              {
                this.state.filteredLists.map((product, index) =>{
                  
                  return(
                    <View style={styles.product_bg}>
                      <Text style={styles.product_title}>{product.name}</Text>
                    </View>
                  )
                  
                })
              }
              
            </View>
           </ScrollView>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  main_content: {
    backgroundColor:'#f9f9f9',
    // height:'100%',
    paddingBottom: 60,
    borderTopLeftRadius:30,
    borderTopRightRadius:30,
    overflow: 'hidden',
    
    // padding:10,
    // marginBottom:85,
    flexGrow: 1,
    // paddingBottom:40
},
  search_input:{
    backgroundColor:colors.white,
    borderRadius:5,
    paddingHorizontal:15,
    marginVertical:15,
    marginHorizontal:10
  },
  product_bg: {
    backgroundColor:colors.white,
    paddingVertical:15,
    paddingHorizontal:10,
    marginBottom:5,
  },
  product_title: {
    fontSize:17
  }
});