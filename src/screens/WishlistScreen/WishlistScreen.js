import React, { Component } from "react";
import { ActivityIndicator, Image, SafeAreaView, ScrollView, Text, TouchableOpacity, View } from "react-native";
import { Icon } from "react-native-elements";
import Swipeout from "react-native-swipeout";
import { remove_Wishlist, show_Wishlist } from "../../Api/wishlist.api";
import CartBtn from "../../cart";
import Header from "../../components/common/Header";
import { colors } from "../../configs/colors.config";
import { url } from "../../configs/constants.config";
import { styles } from "./styles";

export default class WishlistScreen extends Component{
    state = {
        products: [],
        cartItems: {}
    }

    showWishlist = async () => {
        let data = {
            language_id : 1,
        }
        let response = await show_Wishlist(data);
        
        if(response.status) {
          this.setState({products: response.data.data});
         }
      }
      async componentDidMount () {
        this.showWishlist();
        
      }
      removeWishlist = async (products_id, favorite_id) => {
        let data = {
          favorite_id :  favorite_id,
          product_id  : products_id
        }
        // console.log('data', data);
        let response = await remove_Wishlist(data);
        
        if(response.status) {
          // this.setState({addWishListItem: response.data});
         }
        
      }
    
    render() {
        let {navigation} = this.props;
        let swipeBtns = [            
            {
             component: (
               <View
                   style={{
                     flex: 1,
                     alignItems: 'center',
                     justifyContent: 'center',
                     flexDirection: 'column',
                     backgroundColor:'red',
                     borderRadius:5,
                     margin:10,
                   }}
               >
                 <Icon
                     name='trash-outline'
                     type='ionicon'
                     color='#fff'
                     style={{ textAlign:"left" }}
                     />
               </View>
             ),
             backgroundColor: 'transparent',
             underlayColor: 'rgba(0, 0, 0, 0, 0)',
             
             onPress: () => {
               console.log("Delete Item");
             },
           },
        ];
        setTimeout(() => {
            this.setState({ loading: true })
          }, 1000)
          if (this.state.loading) {
        return (
                    <View style={{backgroundColor:'#27ae60',headerMode: 'screen', display: 'flex', flexDirection: 'column', height: '100%'}}>
                        <Header {...this.props} />
                        <View style={[styles.main_content]}>
                            <ScrollView>
                                <View style={{padding:10}}>
                            {
                                this.state.products.map((product, id) => {
                                    
                                    return(
                                        <>
                                        {
                                            product.favorite == 'yes'
                                            ?
                                            <Swipeout right={
                                                [            
                                                    {
                                                     component: (
                                                       <View
                                                           style={{
                                                             flex: 1,
                                                             alignItems: 'center',
                                                             justifyContent: 'center',
                                                             flexDirection: 'column',
                                                             backgroundColor:'red',
                                                             borderRadius:5,
                                                             margin:10,
                                                           }}
                                                       >
                                                         <Icon
                                                             name='trash-outline'
                                                             type='ionicon'
                                                             color='#fff'
                                                             style={{ textAlign:"left" }}
                                                             />
                                                       </View>
                                                     ),
                                                     backgroundColor: 'transparent',
                                                     underlayColor: 'rgba(0, 0, 0, 0, 0)',
                                                     
                                                     onPress: () => {
                                                        this.removeWishlist(product.products_id, product.favorite_id)
                                                    },
                                                   },
                                                ]
                                            }
                                            Close
                                            backgroundColor= 'transparent'>
                                                <View style={styles.wishlist_content} key={id}>
                                                    <View style={{flexDirection:'row'}}>
                                                        <View style={{flex:.3}}>
                                                            <Image style={styles.product_image} source={{uri: url + product.path }} />
                                                        </View>
                                                        <View style={{flex:.6,justifyContent:'center',paddingHorizontal:10}}>
                                                            {/* <Text style={{color:colors.darkgrey}}>{product.categories_name}</Text> */}
                                                            <Text style={styles.product_title}>{product.name}</Text>
                                                            <Text style={{color:colors.primary}}>{product.products_price}</Text>
                                                            {/* <TouchableOpacity>
                                                                <Text style={{backgroundColor:colors.primary,color:colors.white,textAlign:'center',padding:8,borderRadius:5}}>ADD TO CART</Text>
                                                            </TouchableOpacity> */}
                                                        </View>
                                                        {/* <View style={{flex:.1,justifyContent:'flex-end',paddingHorizontal:10}}>
                                                        <TouchableOpacity onPress={() => this.updateCart('plus')}>
                                                            <View style={styles.add_cart_btn}>
                                                                <Icon type="antdesign" color={colors.white} name="plus" />
                                                            </View>
                                                        </TouchableOpacity>
                                                        </View> */}
                                                        <CartBtn {...this.props} id={id} product={product} />
                                                    </View>
                                                </View>
                                            </Swipeout>
                                            :
                                            <></>
                                        }
                                        </>
                                        
                                    );
                                })
                                
                            }  
                            </View>
                            </ScrollView>                          
                        </View>
                    </View>
        );
    } else {
        return (
          <View style={{flex : 1, justifyContent: 'center', alignItems: 'center',}}>                            
            <ActivityIndicator  color={colors.primary} />
          </View>
          );
      }
    }
}