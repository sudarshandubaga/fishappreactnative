import { StyleSheet } from "react-native";
import { colors } from "../../configs/colors.config";

export const styles = StyleSheet.create({
    scrollView: {

    },
    navigation_title: {
        color:colors.white,
        fontSize: 25,
        marginVertical:18,
        justifyContent:'center',
        
    },
    main_content: {
        backgroundColor:'#f9f9f9',
        // height:'100%',
        paddingBottom: 60,
        borderTopLeftRadius:30,
        borderTopRightRadius:30,
        overflow: 'hidden',
        
        // padding:10,
        // marginBottom:85,
        flexGrow: 1,
        // paddingBottom:40
    },
    wishlist_content: {
        marginVertical:5,
        backgroundColor: colors.white,
        borderRadius:5,
        padding:10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,

        elevation: 3,
    },
    product_image: {
        width:75,
        height:75,
        borderRadius:10,
        resizeMode:'contain'
    },
    product_title: {
        fontSize:17,
        fontWeight:'bold'
    },
    add_cart_btn: {
        backgroundColor: colors.primary,
        // alignItems:'center',
        borderRadius: 15,
        width: 30,
        height: 30,
        justifyContent:'center'
      },
});