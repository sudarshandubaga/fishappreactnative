import { StyleSheet } from "react-native";
import { color } from "react-native-elements/dist/helpers";
import { colors } from "../../configs/colors.config";

export const styles = StyleSheet.create({
    scrollView: {

    },
    main_content: {
        backgroundColor:'#f9f9f9',
        // height:'100%',
        paddingBottom: 60,
        borderTopLeftRadius:30,
        borderTopRightRadius:30,
        overflow: 'hidden',
        
        // padding:10,
        // marginBottom:85,
        flexGrow: 1,
        // paddingBottom:40
    },    
    product_title: {
        fontSize:17,
        fontWeight:'bold'
    },
    profile_pic_content: {
        marginVertical:40,
        justifyContent:'center',
        alignItems:'center',
    },
    profile_pic_brdr: {
        borderColor:colors.bgColor, 
        padding:3,
        borderWidth:3,
        // border-style: dotted solid double
        // borderTopColor:'transparent',
        borderRadius:50,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    product_image: {
        width:80,
        height:80,
        borderRadius:50,
    },
    profile_name: {
        fontSize: 22,
        color: colors.white,
        marginTop:20
    },
    profile_menu_box: {
        marginVertical:5,
        backgroundColor: colors.white,
        borderRadius:5,
        padding:10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,

        elevation: 3,
    },
    profile_point_title:{
        fontSize:19,
        color:colors.primary
    },
    show_address_title: {
        fontSize:19,
        // color:colors.darkgrey
    },
    acc_left_name: {
        fontSize:17
    },
    acc_right_name: {
        fontSize:19
    },
    edit_profile: {
        // marginVertical:10,
        backgroundColor:colors.primary,
        borderRadius:5,
        alignItems:'flex-end',
        width:60,
        padding:5,
        color:colors.white,
        textAlign:'center',
        fontSize:17,
        textTransform:'uppercase',
        fontWeight:'bold'
        
    },
    add_address_btn_position: {
        position:'absolute',
       bottom:'20%',
        right:'5%',
    },
    add_address_btn: {
        backgroundColor: colors.primary,
        // alignItems:'center',
        borderRadius: 50,
        width: 60,
        height: 60,
        justifyContent:'center',
        
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.29,
        shadowRadius: 4.65,
        
        elevation: 7,
        
    },
    location: {
        backgroundColor: 'white',
        margin: 25
      },
    addr_input: {
        borderWidth:1,
        borderColor:colors.darkgrey,
        borderRadius:5,
        fontSize:17,
        // fontWeight:'bold',
        padding:4,
        color:'black',
        marginBottom:5
    },
    register_button: {
        // marginVertical:10,
        backgroundColor:colors.primary,
        paddingVertical:8,
        borderRadius:5,
        color:colors.white,
        textAlign:'center',
        fontSize:19
      },
    profile_dp: {
        borderRadius:50,
        height:100,
        width:100,        
    },
    change_pic_title: {
        fontSize:17,
        color:colors.primary,
        paddingVertical:8,
        marginVertical:5
    },
    edit_profile_input: {
        borderWidth:.2,
        borderColor:colors.darkgrey,
        borderRadius:2,
        fontSize:17,
        // fontWeight:'bold',
        padding:4,
        color:'black',
        marginBottom:5,
        backgroundColor:colors.white
    },
});