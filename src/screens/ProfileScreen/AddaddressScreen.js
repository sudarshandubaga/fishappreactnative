import React, { Component } from "react";
import { ScrollView, Text, View, Picker } from "react-native";
import { Icon } from "react-native-elements";
import Header from "../../components/common/Header";
import { styles } from "./styles";
// import RNGooglePlacePicker from 'react-native-google-place-picker';
import { TouchableOpacity } from "react-native";
import { TextInput } from "react-native";
import { Input } from "react-native-elements/dist/input/Input";
import { colors } from "../../configs/colors.config";
import { add_address, cities, update_address } from "../../Api/address.api";
import AsyncStorage from "@react-native-async-storage/async-storage";

export default class AddaddressScreen extends Component{
    constructor(props) {
        super(props);
        this.state = {
          pin_code: '',
          house_no:  '',
          entry_street_address: '',
          landmark: '',
          city:'',
          entry_latitude:'',
          entry_longitude: '',
          cities : [],
        }
    }
    addAddress = async () => {
        
          let data = {
              pin_code              : this.state.pin_code,
              house_no              : this.state.house_no,
              entry_street_address  : this.state.entry_street_address,
              landmark              : this.state.landmark,
              city                  : this.state.city,
              entry_latitude        : this.state.entry_latitude,
              entry_longitude       : this.state.entry_longitude
          }
         
        let response = await add_address(data);

        this.props.navigation.navigate('Address'); 
    }

    replace = async () => {
        let {route} = this.props;
        let addr = route.params;
        this.setState({pin_code: addr.postcode});
        this.setState({house_no: addr.house_no});
        this.setState({entry_street_address: addr.street});
        this.setState({landmark: addr.landmark});
        this.setState({city: addr.city});
        this.setState({entry_latitude: addr.latitude});
        this.setState({entry_longitude: addr.longitude});
    }
    
    updateAddress = async () => {
        let {route} = this.props;
        let addr = route.params;
        let data = {
                address_id            : addr.address_id,
                pin_code              : this.state.pin_code,
                house_no              : this.state.house_no,
                entry_street_address  : this.state.entry_street_address,
                landmark              : this.state.landmark,
                city                  : this.state.city,
                entry_latitude        : this.state.entry_latitude,
                entry_longitude       : this.state.entry_longitude
        }
        console.log('data',data);
        let response = await update_address(data);
        this.props.navigation.navigate('Address');
        
    }

    City_list = async () =>  {
        let response = await cities();
        if(response.status) {
            this.setState({cities: response.data.data});            
            
        }
    }

    async componentDidMount () {
        this.replace();
        this.City_list();
        // let users = await AsyncStorage.getItem('@user');
        // users = users !== null ? JSON.parse(users) : {};
        // this.setState({user: users.user_details});
    }
    
    //   onPress() {
    //     RNGooglePlacePicker.show((response) => {
    //       if (response.didCancel) {
    //         console.log('User cancelled GooglePlacePicker');
    //       }
    //       else if (response.error) {
    //         console.log('GooglePlacePicker Error: ', response.error);
    //       }
    //       else {
    //         this.setState({
    //           location: response
    //         });
    //       }
    //     })
    //   }
    
    render() {
        
        let {route} = this.props;
        console.log('id :',route.params);

        
        return(
            <View style={{backgroundColor:'#27ae60',headerMode: 'screen', display: 'flex', flexDirection: 'column', height: '100%' }}>
                <Header {...this.props} />
                <View style={styles.main_content}>                            
                    <ScrollView>
                        {/* <View style={{padding: 10}}>
                        <TouchableOpacity onPress={this.onPress.bind(this)}>
                            <Text style={{color: '#72c02c', fontSize: 20, fontWeight:'bold'}}>
                                Click me to push Google Place Picker!
                            </Text>
                        </TouchableOpacity>
                        <View style={styles.location}>
                            <Text style={{color: 'black', fontSize: 15}}>
                                {JSON.stringify(this.state)}
                            </Text>
                        </View> 
                        </View>                         */}
                        <View style={{padding: 10}}>
                            <View style={{marginTop:5}}>
                                <TextInput
                                    style={styles.addr_input}
                                    placeholder="Pin Code *"
                                    keyboardType="numeric"
                                    value={this.state.pin_code}  
                                    onChangeText={pin_code => this.setState({ pin_code })}
                                />
                                <TextInput
                                style={styles.addr_input}
                                    placeholder="Flat/House No."
                                    value={this.state.house_no}  
                                    onChangeText={house_no => this.setState({ house_no })}
                                />
                                {/* <TextInput
                                style={styles.addr_input}
                                    placeholder="Floor No."

                                />
                                <TextInput
                                style={styles.addr_input}
                                    placeholder="Tower No."
                                />
                                <TextInput
                                style={styles.addr_input}
                                    placeholder="Building/Apartment Name"
                                /> */}
                                <TextInput
                                style={styles.addr_input}
                                    placeholder="Address *"
                                    value={this.state.entry_street_address}  
                                    onChangeText={entry_street_address => this.setState({ entry_street_address })}
                                />
                                <TextInput
                                style={styles.addr_input}
                                    placeholder="Landmark/Area"
                                    value={this.state.landmark}  
                                    onChangeText={landmark => this.setState({ landmark })}
                                />
                                {/* <TextInput
                                style={styles.addr_input}
                                    placeholder="City"
                                    value={this.state.city}  
                                    onChangeText={city => this.setState({ city })}
                                /> */}
                                <View style={styles.addr_input} >
                                <Picker
                                    style={{height:27}}
                                    selectedValue={this.state.city}
                                    // onChangeText={city => this.setState({ city })}
                                    onValueChange={city => this.setState({city})}
                                >
                                    {
                                        this.state.cities.map((city, index) => {
                                            return(
                                                <Picker.Item label={city.name} value={city.id} />
                                            );
                                        })
                                    }
                                </Picker>
                                </View>
                                <TextInput
                                style={styles.addr_input}
                                    placeholder="Latitude"
                                    value={this.state.entry_latitude}  
                                    onChangeText={entry_latitude => this.setState({ entry_latitude })}
                                />
                                <TextInput
                                style={styles.addr_input}
                                    placeholder="Longitude"
                                    value={this.state.entry_longitude}  
                                    onChangeText={entry_longitude => this.setState({ entry_longitude })}
                                />
                                {
                                    route.params
                                    ?
                                    <TouchableOpacity onPress={() =>   this.updateAddress()}>
                                        <Text style={styles.register_button}>Update</Text>
                                    </TouchableOpacity> 
                                    :
                                    <TouchableOpacity onPress={() =>   this.addAddress()}>
                                        <Text style={styles.register_button}>ADD</Text>
                                    </TouchableOpacity>


                                }
                                
                            </View>
                        </View>
                    </ScrollView>  
                </View>
            </View>
        );
    }
}