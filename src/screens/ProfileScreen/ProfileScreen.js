import React, { Component } from "react";
import { Icon } from 'react-native-elements';
import {
    Image,
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    View,  
    TextInput,
    TouchableOpacity,
  } from 'react-native';
import Header from "../../components/common/Header";
import { styles } from "./styles";
import { colors } from "../../configs/colors.config";
import { logout } from "../../Api/auth.api";
import AsyncStorage from "@react-native-async-storage/async-storage";

export default class ProfileScreen extends Component{

    logout = async () => {
        
        let response = await logout();
        // console.log('res',response);
         if(response.status) {
           this.setState({res: response.data});
           let remove_user = await AsyncStorage.removeItem('@user');
           let remove_token = await AsyncStorage.removeItem('@token');
           this.props.navigation.navigate('Login'); 
          }
    }
    async componentDidMount () {
    
        // this.logout();
      }


    render() {
        let {navigation} = this.props;
        return(
                <View style={{backgroundColor:'#27ae60',headerMode: 'screen', display: 'flex', flexDirection: 'column', height: '100%'}}>
                     <Header {...this.props} />

                    <View style={styles.profile_pic_content}>
                        <View style={styles.profile_pic_brdr}>
                            <Image style={styles.product_image} source={require('../../../imgs/logo.png')} />
                            {/* <View style={{position:'absolute',borderRadius:50,top:"70%",right:"-3%",backgroundColor:colors.grey,padding:5,borderColor:colors.primary,borderWidth:3}}>
                                <Icon                                         
                                    name='md-camera-outline'
                                    type='ionicon'
                                    iconStyle={{fontSize:17}}
                                    color={colors.white} 
                                />
                            </View> */}
                        </View>
                        <Text style={styles.profile_name}>Shrawan Choudhary</Text>
                    </View>
                    <View style={styles.main_content}>
                    <ScrollView>
                        <View style={{padding: 10}}>
                        <TouchableOpacity onPress={() => navigation.navigate('Account')}>
                            <View style={styles.profile_menu_box}>
                                <View style={{flexDirection:'row'}}>
                                    <View style={{flex:.2,justifyContent:'center'}}>
                                        
                                            <Icon                                         
                                            name='md-person-outline'
                                            type='ionicon'
                                            iconStyle={{fontSize:25}}
                                            color={colors.bgColor} />
                                                                        
                                    </View>
                                    <View style={{flex:.6,justifyContent:'center'}}>
                                        <Text style={styles.profile_point_title}>Account</Text>                                    
                                    </View>
                                    <View style={{flex:.2,justifyContent:'center',paddingHorizontal:10}}>
                                        
                                    </View>
                                    
                                    
                                </View>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => navigation.navigate('Login')}>
                            <View style={styles.profile_menu_box}>
                                <View style={{flexDirection:'row'}}>
                                    <View style={{flex:.2,justifyContent:'center'}}>
                                        
                                            <Icon                                         
                                            name='location-outline'
                                            type='ionicon'
                                            iconStyle={{fontSize:25}}
                                            color={colors.bgColor} />
                                                                        
                                    </View>
                                    <View style={{flex:.6,justifyContent:'center'}}>
                                        <Text style={styles.profile_point_title}>Login</Text>                                    
                                    </View>
                                    <View style={{flex:.2,justifyContent:'center',paddingHorizontal:10}}>
                                        
                                    </View>
                                    
                                    
                                </View>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => navigation.navigate('Registration')}>
                            <View style={styles.profile_menu_box}>
                                <View style={{flexDirection:'row'}}>
                                    <View style={{flex:.2,justifyContent:'center'}}>
                                        
                                            <Icon                                         
                                            name='location-outline'
                                            type='ionicon'
                                            iconStyle={{fontSize:25}}
                                            color={colors.bgColor} />
                                                                        
                                    </View>
                                    <View style={{flex:.6,justifyContent:'center'}}>
                                        <Text style={styles.profile_point_title}>Registration</Text>                                    
                                    </View>
                                    <View style={{flex:.2,justifyContent:'center',paddingHorizontal:10}}>
                                        
                                    </View>
                                    
                                    
                                </View>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => navigation.navigate('Address')}>
                            <View style={styles.profile_menu_box}>
                                <View style={{flexDirection:'row'}}>
                                    <View style={{flex:.2,justifyContent:'center'}}>
                                        
                                            <Icon                                         
                                            name='location-outline'
                                            type='ionicon'
                                            iconStyle={{fontSize:25}}
                                            color={colors.bgColor} />
                                                                        
                                    </View>
                                    <View style={{flex:.6,justifyContent:'center'}}>
                                        <Text style={styles.profile_point_title}>Address</Text>                                    
                                    </View>
                                    <View style={{flex:.2,justifyContent:'center',paddingHorizontal:10}}>
                                        
                                    </View>
                                    
                                    
                                </View>
                            </View>
                        </TouchableOpacity>
                        {/* <TouchableOpacity onPress={() => navigation.navigate('Wishlist')}>
                            <View style={styles.profile_menu_box}>
                                <View style={{flexDirection:'row'}}>
                                    <View style={{flex:.2,justifyContent:'center'}}>
                                        
                                            <Icon                                         
                                            name='create-outline'
                                            type='ionicon'
                                            iconStyle={{fontSize:25}}
                                            color={colors.bgColor} />
                                                                        
                                    </View>
                                    <View style={{flex:.6,justifyContent:'center'}}>
                                        <Text style={styles.profile_point_title}>Edit Profile</Text>                                    
                                    </View>
                                    <View style={{flex:.2,justifyContent:'center',paddingHorizontal:10}}>
                                        
                                    </View>
                                    
                                    
                                </View>
                            </View>
                        </TouchableOpacity>  */}
                        <TouchableOpacity onPress={() => navigation.navigate('Wishlist')}>
                            <View style={styles.profile_menu_box}>
                                <View style={{flexDirection:'row'}}>
                                    <View style={{flex:.2,justifyContent:'center'}}>
                                        
                                            <Icon                                         
                                            name='heart-outline'
                                            type='ionicon'
                                            iconStyle={{fontSize:25}}
                                            color={colors.bgColor} />
                                                                        
                                    </View>
                                    <View style={{flex:.6,justifyContent:'center'}}>
                                        <Text style={styles.profile_point_title}>Wishlist</Text>                                    
                                    </View>
                                    <View style={{flex:.2,justifyContent:'center',paddingHorizontal:10}}>
                                        
                                    </View>
                                    
                                    
                                </View>
                            </View>
                        </TouchableOpacity> 
                        <TouchableOpacity onPress={() => navigation.navigate('Order')}>
                            <View style={styles.profile_menu_box}>
                                <View style={{flexDirection:'row'}}>
                                    <View style={{flex:.2,justifyContent:'center'}}>
                                        
                                            <Icon                                         
                                            name='receipt-outline'
                                            type='ionicon'
                                            iconStyle={{fontSize:25}}
                                            color={colors.bgColor} />
                                                                        
                                    </View>
                                    <View style={{flex:.6,justifyContent:'center'}}>
                                        <Text style={styles.profile_point_title}>Order History</Text>                                    
                                    </View>
                                    <View style={{flex:.2,justifyContent:'center',paddingHorizontal:10}}>
                                        
                                    </View>
                                    
                                    
                                </View>
                            </View>
                        </TouchableOpacity> 
                        <TouchableOpacity onPress={() => navigation.navigate('Cart')}>
                            <View style={styles.profile_menu_box}>
                                <View style={{flexDirection:'row'}}>
                                    <View style={{flex:.2,justifyContent:'center'}}>
                                        
                                            <Icon                                         
                                            name='cart-outline'
                                            type='ionicon'
                                            iconStyle={{fontSize:25}}
                                            color={colors.bgColor} />
                                                                        
                                    </View>
                                    <View style={{flex:.6,justifyContent:'center'}}>
                                        <Text style={styles.profile_point_title}>Cart</Text>                                    
                                    </View>
                                    <View style={{flex:.2,justifyContent:'center',paddingHorizontal:10}}>
                                        
                                    </View>
                                    
                                    
                                </View>
                            </View>
                        </TouchableOpacity> 
                        <TouchableOpacity onPress={() =>   this.logout()}>
                            <View style={styles.profile_menu_box}>
                                <View style={{flexDirection:'row'}}>
                                    <View style={{flex:.2,justifyContent:'center'}}>
                                        
                                            <Icon                                         
                                            name='log-out-outline'
                                            type='ionicon'
                                            iconStyle={{fontSize:25}}
                                            color={colors.bgColor} />
                                                                        
                                    </View>
                                    <View style={{flex:.6,justifyContent:'center'}}>
                                        <Text style={styles.profile_point_title}>Logout</Text>                                    
                                    </View>
                                    <View style={{flex:.2,justifyContent:'center',paddingHorizontal:10}}>
                                        
                                    </View>
                                    
                                    
                                </View>
                            </View>
                        </TouchableOpacity>
                        
                        </View> 
                    </ScrollView>
                    </View>
                </View> 
        );
    }
}