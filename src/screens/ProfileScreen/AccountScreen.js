import AsyncStorage from "@react-native-async-storage/async-storage";
import React, { Component } from "react";
import { TouchableOpacity } from "react-native";
import { TextInput } from "react-native";
import { ActivityIndicator } from "react-native";
import { SafeAreaView, ScrollView, Text, View, Image } from "react-native";
import { Icon } from "react-native-elements";
import { edit_profile, userDetail } from "../../Api/auth.api";
import Header from "../../components/common/Header";
import { colors } from "../../configs/colors.config";
import { styles } from "./styles";
// import PhotoUpload from 'react-native-photo-upload'
// import ImagePicker from 'react-native-image-picker'
import { launchImageLibrary } from 'react-native-image-picker';
import { url } from "../../configs/constants.config";

export default class AccountScreen extends Component{
    state = {
        user_name: null,
        email: null,
        mobile: null,
        user:null,
        user_detail:[],
        photo:null
    }
    createFormData = (photo, body = {}) => {
        const data = new FormData();
        
        if(photo) {
            data.append('avatar', {
                name: photo.fileName,
                type: photo.type,
                uri: Platform.OS === 'ios' ? photo.uri.replace('file://', '') : photo.uri,
            });
        }
        // console.log('photo',photo);
        
        Object.keys(body).forEach((key) => {
            data.append(key, body[key]);
        });
        
        return data;
    }
    user_detail = async() => {
        let response = await userDetail();
        // console.log('res',response.data.user_detail);
        if(response.status){
            this.setState({
                user_detail: response.data.user_detail,
                user_name: response.data.user_detail.user_name,
                email: response.data.user_detail.email,
                mobile: response.data.user_detail.phone,
                photo:response.data.user_detail.avatar 
            });
        }
    }
    editProfile = async() => {
        let data = {
            user_name : this.state.user_name,
            email : this.state.email,
        }
        data = this.createFormData(this.state.photo, data);
        // console.log('abcd',JSON.stringify(data));
        let response = await edit_profile(data);
        // if(response.status){
        //     this.setState({user_name: response.data.data});
        // }
        console.log('response',response);
    }
    handleChoosePhoto = () => {
        launchImageLibrary({ noData: true }, (response) => {
            if (response) {
                this.setState({photo: response});
            }
        });
    }
    async componentDidMount(){
        // this.editProfile();
        this.user_detail();
        let user = await AsyncStorage.getItem('@user');
        user = user !== null ? JSON.parse(user) : {};
        this.setState({
            user
        });
    }
    
    render() {
        let url = 'https://rudrakshatech.com/fishApp/public/images/user/';
        console.log('abcd',url + this.state.photo);
        const { photo } = this.state
        // console.log('data',this.state.user_detail);
        let {navigation} = this.props;
        setTimeout(() => {
            this.setState({ loading: true })
          }, 1000)
          if (this.state.loading) {
        return(
            <View style={{backgroundColor:'#27ae60',headerMode: 'screen', display: 'flex', flexDirection: 'column', height: '100%'}}>
                        <Header {...this.props} />
                        <View style={styles.main_content} >
                            <ScrollView >
                            <View style={{padding:10}}>

                                {/* <View style={{alignItems:'flex-end'}}>
                                    <View style={styles.edit_profile}>
                                        <Icon
                                            name='create-outline'
                                            type='ionicon'
                                            color={colors.white}
                                            style={{ textAlign:"left" }}
                                            // onPress={() => alert('abcd')}    
                                            />
                                        <Text style={{color:colors.white,textAlign:'center'}}>Edit</Text>
                                    </View>
                                </View> */}
                                <View style={{justifyContent:'center',borderRadius:50,overflow:'hidden',alignContent:'center',alignItems:'center',marginTop:90}}>
                                
                                { 
                                    !this.state.photo
                                    ?
                                    <Image style={styles.profile_dp} source={require('../../../imgs/logo.png')} /> 
                                    :
                                    <Image style={styles.profile_dp} source={{uri: url + this.state.photo}} />
                                }
                                    <TouchableOpacity onPress={() => this.handleChoosePhoto()}>
                                        <Text style={styles.change_pic_title}>Change Profile Picture</Text>
                                    </TouchableOpacity>
                                    
                                </View>
                                <View style={{flexDirection:'row',marginVertical:5,paddingHorizontal:10,}}>
                                    {/* <View style={{flex:.2,justifyContent:'center'}}>
                                        <Text style={styles.acc_left_name}>Name</Text>
                                    </View> */}
                                    <View style={{flex:1}}>
                                        {/* <Text style={styles.acc_right_name}>: Shrawan Choudhary</Text> */}
                                        <TextInput
                                            style={styles.edit_profile_input}
                                            placeholder="Username"
                                            value={this.state.user_name}  
                                            onChangeText={user_name => this.setState({ user_name })}
                                        />
                                        {/* <Text>{this.state.user_detail.user_name}</Text> */}

                                    </View>
                                </View>
                                <View style={{flexDirection:'row',marginVertical:5,paddingHorizontal:10,}}>
                                    {/* <View style={{flex:.2,justifyContent:'center'}}>
                                        <Text style={styles.acc_left_name}>Email</Text>
                                    </View> */}
                                    <View style={{flex:1}}>
                                        {/* <Text style={styles.acc_right_name}>: Rams50288@gmail.com</Text> */}
                                        <TextInput
                                            style={styles.edit_profile_input}
                                            placeholder="Email"
                                            value={this.state.email ?? this.state.user_detail.email}  
                                            onChangeText={email => this.setState({ email })}
                                        />
                                    </View>
                                </View>
                                <View style={{flexDirection:'row',marginVertical:5,paddingHorizontal:10,}}>
                                    {/* <View style={{flex:.2,justifyContent:'center'}}>
                                        <Text style={styles.acc_left_name}>Mobile No.</Text>
                                    </View> */}
                                    <View style={{flex:1}}>
                                        {/* <Text style={styles.acc_right_name}>: 9660257608</Text> */}
                                        <TextInput
                                            style={styles.edit_profile_input}
                                            placeholder="Mobile No"
                                            value={this.state.mobile}  
                                            onChangeText={mobile => this.setState({ mobile })}
                                            editable={false}
                                        />
                                    </View>
                                </View>
                                <View style={{flexDirection:'row',marginVertical:5,paddingHorizontal:10,justifyContent:'flex-end'}}>
                                    <TouchableOpacity onPress={() => this.editProfile()}>
                                        <Text style={styles.edit_profile}>Edit</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </ScrollView>
                        </View>
                    </View>
        );
    } else {
        return (
          <View style={{flex : 1, justifyContent: 'center', alignItems: 'center',}}>                            
            <ActivityIndicator  color={colors.primary} />
          </View>
          );
      }
    }
}