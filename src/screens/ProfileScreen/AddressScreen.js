import React, { Component } from "react";
import { SafeAreaView, ScrollView, Text, TouchableOpacity, View } from "react-native";
import { Icon } from "react-native-elements";
import { colors } from "../../configs/colors.config";
import { styles } from "./styles";
import Header from "../../components/common/Header";
import Swipeout from "react-native-swipeout";
import { addresses, remove_address, update_address } from "../../Api/address.api";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { ActivityIndicator } from "react-native";

export default class AddressScreen extends Component{
    
    state = {
        addresses: [ ]
    }
    Address_list = async () =>  {
        let response = await addresses();
        if(response.status) {
            this.setState({addresses: response.data.data});            
            
        }
    }

    removeAddress = async (id) => {
        let data = {
            address_book_id : id
        }
        let response = await remove_address(data); 
        
    }

    selectAddress = async (addr) => {
        await AsyncStorage.setItem('@selectedAddress', JSON.stringify(addr));
         
        
        this.props.navigation.navigate('Cart');
    }

    updateAddress = async (addr) => {
        data = {
            address_id : addr.id
        }
        let response = await update_address(data);
        this.props.navigation.navigate('Add Address', addr);
        
        
    }

    async componentDidMount () {
        this.Address_list();
        // this.updateAddress();
        this.removeAddress();
        let users = await AsyncStorage.getItem('@user');
        users = users !== null ? JSON.parse(users) : {};

       
        // this.setState({user: users.user_details});
    }

    render() {
        let {navigation} = this.props;

        // let swipeBtns = [
        //     {
        //         component: (
        //           <View
        //               style={{
        //                 flex: 1,
        //                 alignItems: 'center',
        //                 justifyContent: 'center',
        //                 flexDirection: 'column',
        //                 backgroundColor:colors.primary,
        //                 borderRadius:5,
        //                 marginVertical:5,
        //                 marginHorizontal:10
        //               }}
        //           >
        //             <Icon
        //                 name='create-outline'
        //                 type='ionicon'
        //                 color='#fff'
        //                 style={{ textAlign:"left" }}
        //                 />
        //           </View>
        //         ),
        //         backgroundColor: 'transparent',
        //         underlayColor: 'rgba(0, 0, 0, 0, 0)',
                
        //         onPress: () => {
        //           console.log("Edit Item");
        //         },
        //       },
            
        //     {
        //      component: (
        //        <View
        //            style={{
        //              flex: 1,
        //              alignItems: 'center',
        //              justifyContent: 'center',
        //              flexDirection: 'column',
        //              backgroundColor:'red',
        //              borderRadius:5,
        //              marginVertical:5,
        //              marginHorizontal:10
        //            }}
        //        >
        //          <Icon
        //              name='trash-outline'
        //              type='ionicon'
        //              color='#fff'
        //              style={{ textAlign:"left" }}
        //              />
        //        </View>
        //      ),
        //      backgroundColor: 'transparent',
        //      underlayColor: 'rgba(0, 0, 0, 0, 0)',
             
        //      onPress: () => {
        //        console.log("Delete Item");
        //      },
        //    },
           
        //    ];
        setTimeout(() => {
            this.setState({ loading: true })
            }, 1000)
            if (this.state.loading) {
        return(
                    <View style={{backgroundColor:'#27ae60',headerMode: 'screen', display: 'flex', flexDirection: 'column', height: '100%' }}>
                        <Header {...this.props} />
                        <View style={styles.main_content}>
                                    
                            <ScrollView>
                                <View style={{padding: 10}}>
                                    {
                                        this.state.addresses.map((addr,i) => {
                                            return (
                                            
                                                <Swipeout right={ [
                                                    {
                                                    component: (
                                                        <View
                                                            style={{
                                                            flex: 1,
                                                            alignItems: 'center',
                                                            justifyContent: 'center',
                                                            flexDirection: 'column',
                                                            backgroundColor:colors.primary,
                                                            borderRadius:5,
                                                            marginVertical:5,
                                                            marginHorizontal:10
                                                            }}
                                                        >
                                                        <Icon
                                                            name='create-outline'
                                                            type='ionicon'
                                                            color='#fff'
                                                            style={{ textAlign:"left" }}
                                                            />
                                                        </View>
                                                    ),
                                                    backgroundColor: 'transparent',
                                                    underlayColor: 'rgba(0, 0, 0, 0, 0)',
                                                    
                                                    onPress: () => {
                                                        this.updateAddress(addr)
                                                        },
                                                    },
                                                
                                                {
                                                component: (
                                                    <View
                                                        style={{
                                                        flex: 1,
                                                        alignItems: 'center',
                                                        justifyContent: 'center',
                                                        flexDirection: 'column',
                                                        backgroundColor:'red',
                                                        borderRadius:5,
                                                        marginVertical:5,
                                                        marginHorizontal:10
                                                        }}
                                                    >
                                                    <Icon
                                                        name='trash-outline'
                                                        type='ionicon'
                                                        color='#fff'
                                                        style={{ textAlign:"left" }}
                                                        />
                                                    </View>
                                                ),
                                                backgroundColor: 'transparent',
                                                underlayColor: 'rgba(0, 0, 0, 0, 0)',
                                                
                                                onPress: () => {
                                                    this.removeAddress(addr.address_id)
                                                    },
                                                }
                                                ]}
                                                Close
                                                backgroundColor= 'transparent'>
                                                    <TouchableOpacity onPress={() => this.selectAddress(addr)}> 
                                                        <View style={styles.profile_menu_box} key={i}>
                                                            <View style={{flexDirection:'row'}}>
                                                                <View style={{flex:.2,justifyContent:'center'}}>
                                                                    
                                                                        <Icon                                         
                                                                        name='location-outline'
                                                                        type='ionicon'
                                                                        iconStyle={{fontSize:25}}
                                                                        color={colors.bgColor} />
                                                                                                    
                                                                </View>
                                                                <View style={{flex:.8,justifyContent:'center'}}>
                                                                    <Text style={styles.show_address_title}>{addr.house_no}, {addr.street}, {addr.landmark}, {addr.city}, {addr.postcode}</Text>                                    
                                                                </View>
                                                            </View>
                                                        </View>
                                                    </TouchableOpacity>
                                                </Swipeout>
                                            
                                            );
                                        })
                                    }
                                </View>
                                
                            </ScrollView>
                            <View style={styles.add_address_btn_position}>
                                <TouchableOpacity onPress={() => navigation.navigate('Add Address')}>
                                    <View style={styles.add_address_btn}>
                                        <Icon type="antdesign" color={colors.white} name="plus" />
                                    </View>
                                </TouchableOpacity>
                            </View>
                            
                        </View>
                    </View>
        );
    } else {
        return (
          <View style={{flex : 1, justifyContent: 'center', alignItems: 'center',}}>                            
            <ActivityIndicator  color={colors.primary} />
          </View>
          );
      }
    }
}