import React, { Component } from "react";
import { Icon } from 'react-native-elements';
import {
    Image,
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    View,  
    TextInput,
    TouchableOpacity,
    ActivityIndicator,
  } from 'react-native';
import Bottomnavigation from "../components/Bottomnavigation";
import { get_categories } from "../Api/category.api";
import CategoryBox from "../components/CategoryBox";
import Header from "../components/common/Header";
import { colors } from "../configs/colors.config";

export default class CategoryScreen extends Component {

  state = {
    categories: []
  }

  getchildCategories = async () => {
    let {route} = this.props;
    let response = await get_categories(route.params);
    if(response.status){
        this.setState({categories: response.data});        
    }
  }

  async componentDidMount () {
      this.getchildCategories();
  }


    render () {
        let {navigation,route} = this.props;
      // console.log('abcd',route);
      setTimeout(() => {
        this.setState({ loading: true })
      }, 3000)
      if (this.state.loading) {
        return (

              <View style={{backgroundColor:'#27ae60',headerMode: 'screen',height:'100%'}}>
                <Header {...this.props} />
                <View style={styles.main_content}>
                  <ScrollView>
                    <View style={{padding:10}}>                
                      <View style={styles.cat_content}>                  
                        <View style={styles.home_categories}>
                          {
                            this.state.categories.map((category, index)=>{
                              return(
                                <View style={styles.category_space} key={category.id}>
                                  <TouchableOpacity  onPress={() => navigation.navigate('Products',category.id)}>
                                    <CategoryBox 
                                      data={category}
                                    />  
                                  </TouchableOpacity>
                                </View>
                              );
                            })
                          }                
                        </View>
                      </View>
                    </View>              
                  </ScrollView>
                </View>
              </View>
        );
        } else {
          return (
            <View style={{flex : 1, justifyContent: 'center', alignItems: 'center',}}>                            
              <ActivityIndicator  color={colors.primary} />
            </View>
            );
        }
    }
}


const styles = StyleSheet.create({
    scrollView: {
      // backgroundColor: 'pink',
    },
    navigation_title: {
      color:'#fff',
      fontSize: 25,
      marginVertical:18
    },
    container: {
      flex: 1,
      flexDirection: "column"
    },
    
    main_content: {
      backgroundColor:'#f9f9f9',
      // height:'100%',
      paddingBottom: 60,
      borderTopLeftRadius:30,
      borderTopRightRadius:30,
      overflow: 'hidden',
      
      // padding:10,
      // marginBottom:85,
      flexGrow: 1,
      // paddingBottom:40
  },
    cat_content: {
      backgroundColor: '#fff',
      borderRadius:25,
      padding:15,
      // paddingBottom:40
      
    },
    home_categories: {
      flex: 1,
      flexDirection: 'row',
      flexWrap: 'wrap',
      // justifyContent: 'center',
      alignItems: 'center',
      marginHorizontal: -10 
    },
    category_space: {
        width: '33%',
        paddingHorizontal: 10,
        marginBottom: 15
    },
    cat_header_left: {
      fontSize:20,
      fontWeight:'bold',
      color: '#000',
    },
    cat_header_right: {
      color:'#3bb56e',
      fontSize:16,
      fontWeight:'bold'
    },
    cat_box: {
      width:100,
    },
    cat_box_bg: {
      backgroundColor: '#e9f7ef',
      borderRadius:10,
      
    },
    cat_image: {
      width: 65,
      height: 65,
      // borderRadius:50,
      margin:20,
      resizeMode:'contain'
    },
    cat_title_content: {
      
    },
    cat_title: {
      textAlign:'center',
      fontSize:16,
      marginVertical:10,
      
      fontWeight:'bold'   
    },
    
    sectionTitle: {
      padding: 15,
      color: '#f00',
      fontSize: 24,
      fontWeight: '600',
    },
    sectionDescription: {
      marginTop: 8,
      fontSize: 18,
      fontWeight: '400',
    },
    highlight: {
      fontWeight: '700',
    },
    tinyLogo: {
      width: 50,
      height: 50,
      borderRadius:50
    },
    logo: {
      width: 66,
      height: 58,
    },
    image: {
      flex: 1,
      resizeMode: "cover",
      justifyContent: "center"
    },
    text: {
      color: "white",
      fontSize: 42,
      height:40,
      fontWeight: "bold",
      textAlign: "center",
      backgroundColor: "#000000a0"
    },
  });