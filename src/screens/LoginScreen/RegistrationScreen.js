import AsyncStorage from "@react-native-async-storage/async-storage";
import React, { Component, useState } from "react";
import { AppRegistry, Button, Image, SafeAreaView, ScrollView, StatusBar, Text, TextInput, TouchableOpacity, View } from "react-native";
import { Icon, Input } from "react-native-elements";
import { CheckBox } from 'react-native-elements'
import { register } from "../../Api/auth.api";
import { colors } from "../../configs/colors.config";
import { styles } from "./styles";

export default class RegistrationScreen extends Component{
    constructor(props) {  
        //constructor to set default state  
        super(props);  
        this.state = {  
            username: '',  
            referral_code:'',
            email:'',
        };  

    } 
    

    registerapi = async () => {
        let {route} = this.props;
        let user = this.state;
        let mobile = route.params.mobile;
        let response = await register(user, mobile);
        // console.log('response',response);
        console.log('res',response);
        if(response.data.status){
            
            await AsyncStorage.setItem('@user', JSON.stringify(response.data.user_details));
            await AsyncStorage.setItem('@token', JSON.stringify(response.data.token));
            this.props.navigation.navigate('Home'); 

        }
        //  else {
        //     if(response.data.user_details.data_fill == 0){                
        //         this.props.navigation.navigate('Registration',{mobile}); 
        //     }
        // }

    }
    

    async componentDidMount () {
        // this.otpsend();
    }
    
    render() {
        let {navigation,route} = this.props;
        return(
            <View style={{flex:1,backgroundColor:colors.bgColor,justifyContent:'center',paddingHorizontal:15}}>
                    <StatusBar barStyle='light-content' backgroundColor={colors.bgColor}/>
                    <View>
                        <View style={{alignItems:'center',marginBottom:40}}>
                            <Image style={styles.product_image} source={require('../../../imgs/logo.png')} />
                        </View>
                        <View style={{backgroundColor:colors.white,padding:15,borderRadius:10}}>
                            <Input
                                placeholder='User Name'                            
                                leftIcon={{ type: 'ionicon', name: 'md-person-sharp' }}
                                value={this.state.username}  
                                onChangeText={username => this.setState({ username })}
                                />
                            
                                <Input
                                placeholder="Email"
                                leftIcon={{ type: 'ionicon', name: 'md-mail' }}
                                value={this.state.email}  
                                onChangeText={email => this.setState({ email })}
                                />
                                {/* <TextInput
                                style={styles.search_input}
                                placeholder="Email"
                                // keyboardType="numeric"
                                /> */}
                                <Input
                                placeholder="Mobile No."
                                leftIcon={{ type: 'ionicon', name: 'md-phone-portrait' }}
                                // keyboardType="numeric"
                                value={route.params.mobile}
                                />
                                <Input
                                // style={styles.search_input}
                                leftIcon={{ type: 'ionicon', name: 'gift' }}
                                placeholder="Referral Code"
                                value={this.state.referral_code}  
                                onChangeText={referral_code => this.setState({ referral_code })}
                                // keyboardType="numeric"
                                />
                                <View style={styles.checkboxContainer}>
                                    <CheckBox
                                        value=""
                                        style={styles.checkbox}
                                        />
                                    <Text style={styles.label}>Terms & Conditions</Text>
                                </View>
                                <TouchableOpacity onPress={() =>   this.registerapi()}>
                                    <Text style={styles.register_button}>REGISTER</Text>
                                </TouchableOpacity>                            
                        </View>
                    </View>
                </View>
        );
    }
}