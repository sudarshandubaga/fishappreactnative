import React, { Component } from "react";
import { AppRegistry, Button, Image, SafeAreaView, ScrollView, StatusBar, Text, TextInput, TouchableOpacity, View } from "react-native";
import { Input } from "react-native-elements";
import { sendOtp } from "../../Api/auth.api";
import { colors } from "../../configs/colors.config";
import { styles } from "./styles";

export default class LoginScreen extends Component{

    constructor(props) {  
        //constructor to set default state  
        super(props);  
        this.state = {  
            mobile: '',  
        };  

    }  
    otpsend = async () => {
        let mobile = this.state.mobile;
        let response = await sendOtp(mobile);
        // console.log('res',response);
        if(response.status){
            this.props.navigation.navigate('Otpverify',{mobile});     
        }

    }

    async componentDidMount () {
        // this.otpsend();
    }

    render() {
        let {navigation} = this.props;
        
        
        return(
            <View style={{flex:1,backgroundColor:colors.bgColor,justifyContent:'center',padding:30}}>
                <StatusBar barStyle='light-content' backgroundColor={colors.bgColor}/>
                <View style={{alignItems:'center',marginBottom:40}}>
                    <Image style={styles.product_image} source={require('../../../imgs/logo.png')} />
                </View>
                
                {/* <TextInput
                    style={styles.search_input}
                    placeholder="Mobile No."
                    keyboardType="numeric"
                    />
                <TouchableOpacity onPress={() => navigation.navigate('Otpverify')}>
                    <Text style={styles.send_otp_button}>SEND OTP</Text>
                </TouchableOpacity>                 */}
                <View style={{backgroundColor:colors.white,padding:15,borderRadius:10}}>
                    <Input
                        placeholder="Mobile No."
                        keyboardType="numeric"
                        leftIcon={{ type: 'ionicon', name: 'md-phone-portrait'}}
                        value={this.state.mobile}  
                        onChangeText={mobile => this.setState({ mobile })}
                    />
                    <TouchableOpacity onPress={() =>   this.otpsend()}  >
                        
                        <Text style={styles.send_otp_button}>SEND OTP</Text>
                    </TouchableOpacity> 
                </View> 
               
            </View>
        );
    }
}