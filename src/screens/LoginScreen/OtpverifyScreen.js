import AsyncStorage from "@react-native-async-storage/async-storage";
import React, { Component, useRef } from "react";
import { AppRegistry, Button, Image, SafeAreaView, ScrollView, StatusBar, Text, TextInput, TouchableOpacity, View } from "react-native";
import { Input } from "react-native-elements";
import { verifyOtp } from "../../Api/auth.api";
import { colors } from "../../configs/colors.config";
import { styles } from "./styles";



export default class OtpverifyScreen extends Component{
    constructor(props) {  
        //constructor to set default state  
        super(props);  
        this.state = {  
            otp: '',  
        };  

    }  
    otpvarify = async () => {
        let {route} = this.props;
        let otp = this.state.otp;
        let mobile = route.params.mobile;
        let response = await verifyOtp(otp, mobile);
        // console.log('response',response);
        // console.log('res',response.data.status);
        if(response.data.status){
            
            await AsyncStorage.setItem('@user', JSON.stringify(response.data));
            await AsyncStorage.setItem('@token', response.data.token);
            
            this.props.navigation.navigate('Home'); 
                

        } else {
            if(response.data.user_details.data_fill == 0){                
                this.props.navigation.navigate('Registration',{mobile}); 
            }
        }

    }

    

    async componentDidMount () {
        this.otpvarify();
    }
    render() {
        let {navigation, route} = this.props;
        // console.log('mo',route.params.mobile);
        return(
            <View style={{flex:1,backgroundColor:colors.bgColor,justifyContent:'center',padding:30}}>
                    <StatusBar barStyle='light-content' backgroundColor={colors.bgColor}/>
                    <View>
                        <View style={{alignItems:'center',marginBottom:40}}>
                            <Image style={styles.product_image} source={require('../../../imgs/logo.png')} />
                        </View>
                        {/* <TextInput
                            style={styles.search_input}
                            placeholder="Enter Otp"
                            keyboardType="numeric"
                            />
                        <TouchableOpacity>
                            <Text style={styles.send_otp_button}>VERIFY OTP</Text>
                        </TouchableOpacity> */}
                        <View style={{backgroundColor:colors.white,padding:15,borderRadius:10}}>
                            <Input
                                placeholder="Enter Otp"
                                keyboardType="numeric"
                                leftIcon={{ type: 'ionicon', name: 'md-phone-portrait' }}
                                maxLength={6}
                                value={this.state.otp}  
                                onChangeText={otp => this.setState({ otp })}
                                />
                            <TouchableOpacity onPress={() =>   this.otpvarify()}>
                                <Text style={styles.send_otp_button}>VERIFY OTP</Text>
                            </TouchableOpacity> 
                        </View> 
                    </View>
                </View>
        );
    }
}