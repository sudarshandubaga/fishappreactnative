import { StyleSheet } from "react-native";
import { colors } from "../../configs/colors.config";

export const styles = StyleSheet.create({
    product_image: {
        width:100,
        height:100,
        borderRadius:50,
        alignItems:'center'
    },
    search_input:{
        width:'100%',
        backgroundColor:colors.white,
        borderRadius:5,
        // borderBottomColor:colors.darkgrey,
        // borderBottomWidth:2,
        paddingHorizontal:15,
        marginVertical:10,
        fontSize:19
      },
      send_otp_button: {
        marginVertical:10,
        backgroundColor:colors.primary,
        paddingVertical:15,
        borderRadius:5,
        color:colors.white,
        textAlign:'center',
    //   fontSize:19
      },
      checkboxContainer: {
        flexDirection: "row",
        marginVertical:0,
        
        // marginBottom: 20,
      },
      checkbox: {
        // alignSelf: "center",
        // margin:0,
        // padding:0
      },
      label: {
        //   justifyContent:'center'
        // margin: 8,
        marginVertical:15
      },
      register_button: {
        // marginVertical:10,
        backgroundColor:colors.primary,
        paddingVertical:15,
        borderRadius:5,
        color:colors.white,
        textAlign:'center',
    //   fontSize:19
      },
});