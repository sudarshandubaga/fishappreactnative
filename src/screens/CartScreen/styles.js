import { StyleSheet } from "react-native";
import { colors } from "../../configs/colors.config";

export const styles = StyleSheet.create({
    scrollView: {

    },
    navigation_title: {
        color:colors.white,
        fontSize: 25,
        marginVertical:18,
        justifyContent:'center',
        
    },
    main_content: {
        backgroundColor:'#f9f9f9',
        // height:'100%',
        paddingBottom: 60,
        borderTopLeftRadius:30,
        borderTopRightRadius:30,
        overflow: 'hidden',
        
        // padding:10,
        // marginBottom:85,
        flexGrow: 1,
        // paddingBottom:40
    },
    product_image: {
        width:75,
        height:75,
        borderRadius:10,
        resizeMode:'stretch'
    },
    product_title: {
        fontSize:17,
        fontWeight:'bold'
    },
    coupon_input: {
        backgroundColor:colors.white,
        fontSize:19,
        padding:10,
        height:40,
        borderBottomLeftRadius:5,
        borderTopLeftRadius:5
    },
    coupon_apply: {
        backgroundColor:colors.primary,
        color:colors.white,
        height:40,
        padding:10,
        fontSize:19,
        textAlign:'center',
        justifyContent:'center',
        borderBottomRightRadius:5,
        borderTopRightRadius:5
    },
    coupon_subtotal:{
        fontSize:21,
        // fontWeight:'bold'
        color:colors.primary
    },
    coupon_subtotal_price: {
        fontSize:27,
        fontWeight:'bold',
        textAlign:'right'
    },
    coupon_discount_price: {
        fontSize:19,
        // fontWeight:'bold',
        textAlign:'right'
    },
    checkout_btn: {
        backgroundColor:colors.primary,
        color:colors.white,
        height:40,
        padding:10,
        fontSize:19,
        textAlign:'center',
        justifyContent:'center',
        fontWeight:'bold'
        // borderRadius:5,
    },
    coupon_total: {
        color: colors.black,
        fontSize:21,
        fontWeight: 'bold'
    },
    coupon_title: {
        fontSize:17,        
        borderColor:colors.darkgrey,
        borderStyle: 'dashed',
        textTransform:'uppercase',
        borderWidth: 1,
        borderRadius: 1,
        fontWeight:'bold',
        letterSpacing:2,
        justifyContent:'center',
        textAlign:'center',
        paddingVertical:5
    },
    coupon_apply_btn: {
        // backgroundColor:colors.primary,
        // color:colors.white,
        // height:40,
        // padding:10,
        fontSize:20,
        textAlign:'right',
        justifyContent:'center',
        color:colors.primary
        // borderRadius:5,
    },
    coupon_list: {
        flexDirection:'row',
        backgroundColor:colors.white,
        borderRadius:5,
        overflow:'hidden', 
        marginBottom: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,        
        elevation: 5
    },
    offer_price: {
        textAlign:'center',
        fontSize:20,
        fontWeight:'bold',
        margin:5
    },
    coupon_remove: {
        backgroundColor:colors.white,
        // backgroundColor:'red',
        color:colors.white,
        height:40,
        padding:10,
        fontSize:19,
        textAlign:'center',
        justifyContent:'center',
        borderBottomRightRadius:5,
        borderTopRightRadius:5
    },
    select_addrs_content: {
        borderWidth:.5,
        borderColor: colors.grey,
        width:'100%',
        borderRadius:5
    },
    select_addrs_title: {
        fontSize:19,
        letterSpacing:1,
        color:colors.darkgrey
        
    },
    profile_menu_box: {
        marginVertical:5,
        backgroundColor: colors.white,
        borderRadius:5,
        overflow:'hidden'
        // padding:10,
        // shadowColor: "#000",
        // shadowOffset: {
        //     width: 0,
        //     height: 1,
        // },
        // shadowOpacity: 0.22,
        // shadowRadius: 2.22,

        // elevation: 3,
    },
    show_address_title: {
        fontSize:14,
        // color:colors.darkgrey
    },
    last_total_pay: {
        backgroundColor:colors.lightBgColor,
        height:40,
        justifyContent:'center',
        alignItems:'center',
    },
    last_total_price:{
        fontSize:26,
        fontWeight:'bold',
    },
    cart_footer_btn: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,        
        elevation: 5
    },
    cart_info: {
        // borderWidth:.2,
        // borderColor:colors.darkgrey,
        backgroundColor:colors.white,
        // borderRadius:5,
        fontSize:17,
        height:30,
        // fontWeight:'bold',
        padding:4,
        textAlignVertical: 'top'
        // color:'black',
        // marginBottom:5
    },
    pay_method_content: {
        backgroundColor:colors.white,
        flexDirection:'row',
        flexWrap: 'wrap',
        // justifyContent:'center',
        paddingHorizontal:10,
        paddingVertical:15
    }
});