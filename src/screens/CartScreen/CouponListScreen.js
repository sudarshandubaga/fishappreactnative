import React, { Component } from "react";
import {
    SafeAreaView,
    ScrollView,
    StatusBar,
    Text,
    View,  
    TextInput,
    TouchableOpacity,
    Image,
    Button,
    ActivityIndicator,
  } from 'react-native';
import { Icon } from "react-native-elements";
import Header from "../../components/common/Header";
import { colors } from "../../configs/colors.config";
import { styles } from "./styles";
import { get_Couponlist } from "../../Api/cart.api";
import AsyncStorage from "@react-native-async-storage/async-storage";


export default class CouponListScreen extends Component{
    
    
    state = {
        coupons: null,
        coupon_code:null
    }

    getCouponlist = async () => {
        let response = await get_Couponlist();
        if(response.status){
            this.setState({coupons: response.data.data});
        }
    }

    couponstore = async (coupon) => {
            
            await AsyncStorage.setItem('@coupon', JSON.stringify(coupon));
            await AsyncStorage.setItem('@coupon_code', JSON.stringify(coupon.code));
            
            this.props.navigation.navigate('Cart');  

    }

    async componentDidMount () {        
        this.getCouponlist();
        this.couponstore();
    }
    
    
    render() {
        console.log('asfsg',this.state.coupons);
        let {navigation} = this.props;
        
        return(
                
                    <View style={{backgroundColor:'#27ae60',headerMode: 'screen', display: 'flex', flexDirection: 'column', height: '100%'}}>
                        <Header {...this.props} />
                        <View style={[styles.main_content]}>
                        <ScrollView>
                            <View style={{padding:10}}>
                        {
                         this.state.coupons ?
                          this.state.coupons.length ? this.state.coupons.map((coupon, index) => {
                             
                        return (
                            
                            <View style={styles.coupon_list} >
                                
                                <View style={{flex:.2,backgroundColor:'orange',borderColor: 'orange', borderStyle: 'dotted', borderWidth: 2,
                                    borderRadius: 1,justifyContent:'center'}}>
                                    {
                                    coupon.discount_type == "percent"
                                    ?
                                    <Text style={styles.offer_price}>{coupon.amount}% OFF</Text>
                                    :
                                    <Text style={styles.offer_price}>₹{coupon.amount} OFF</Text>
                                        
                                    }
                                    
                                </View>
                                <View style={{flex:.5,justifyContent:'center',paddingVertical:10,paddingLeft:20}}>
                                    <Text style={styles.coupon_title}>{coupon.code}</Text>
                                    <Text style={{paddingVertical:5,color:colors.primary}}>T&C</Text>
                                </View>
                                <View style={{flex:.3,justifyContent:'center',paddingHorizontal:10}}>
                                    <TouchableOpacity onPress={() =>   this.couponstore(coupon)}>
                                    {/* <TouchableOpacity onPress = {() => navigation.navigate('Cart',{detail:coupon})}> */}
                                        <View>
                                            <Text style={styles.coupon_apply_btn}>Apply</Text>
                                        </View>
                                    </TouchableOpacity>
                                    
                                </View>
                            </View>
                           );
                        }) : <View style={{justifyContent:'center'}}>
                            <Text style={{textAlign:'center',fontSize:20,paddingVertical:20}}>No Reacords</Text>
                        </View>
                        : 
                        <View style={{flex : 1, justifyContent: 'center', alignItems: 'center',}}>                            
                            <ActivityIndicator  color={colors.primary} />
                        </View>
                      }
                      
                        </View>
                    </ScrollView>
                    </View>
                </View>
        );
    }
}