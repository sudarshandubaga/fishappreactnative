import AsyncStorage from "@react-native-async-storage/async-storage";
import React, { Component } from "react";
import {
    SafeAreaView,
    ScrollView,
    StatusBar,
    Text,
    View,  
    TextInput,
    TouchableOpacity,
    Image,
    Button,
  } from 'react-native';
import { Icon } from "react-native-elements";
import Swipeout from 'react-native-swipeout';
import Header from "../../components/common/Header";
import { colors } from "../../configs/colors.config";
import { styles } from "../CartScreen/styles";
import { url } from "../../configs/constants.config";
import { ActivityIndicator } from "react-native";
import RadioGroup from 'react-native-radio-buttons-group';
import { Input } from "react-native-elements/dist/input/Input";
import { add_order } from "../../Api/order.api";


export default class CartScreen extends Component{
    
    
    state = {
        products: '',
        cart_count:'',
        subTotal: 0,
        cartItems: {},
        coupon: {},
        coupon_code: null,
        selectedaddress : null,
        lastRefresh: Date(Date.now()).toString(),
        radioButtonsData: [{
            id: '1', // acts as primary key, should be unique and non-empty string
            label: 'COD',
            selected: true
        }, {
            id: '2',
            label: 'Online'
        }],
        order_information: '',
        discount: '',
    }
    
    getCoupon = async () => {
        
            // await AsyncStorage.setItem('@token', response.data.token);
            
            // this.props.navigation.navigate('Home'); 
                

        }

    
    getCart = async () => {
        let cartItems = await AsyncStorage.getItem('@cart_items');
        cartItems = cartItems !== null ? JSON.parse(cartItems) : '';
        this.setState({cart_count: Object.keys(cartItems).length});
        this.setState({products: cartItems});
        return cartItems;
        // console.log('products: ', this.state.products);
    }

    
    

    saveCart = async cartItems => {
        
        this.setState({cartItems});
        try {
            await AsyncStorage.setItem('@cart_items', JSON.stringify(cartItems));
        } catch (err) {
            console.log('async error: ', err);
        }
    }

    
    removeProduct = async (id) => {
        let cartItems = await this.getCart();
        
        delete cartItems[id]               
        this.saveCart(cartItems);
        
    }
    updateCart = async (id, type) => {
        
        let cartItems = await this.getCart();
        let qty = cartItems[id].qty;
            qty = parseInt(qty);
    
        if(type === 'plus') {
            qty += 1;
        } else {
            qty -= 1;
        }
    
        if(qty > 0)
            cartItems[id].qty = qty;
        else
            delete cartItems[id]

               
        this.saveCart(cartItems);
    }
    couponforgot = async () => {
        let ccoupon_code_remove = await AsyncStorage.removeItem('@coupon_code');
        let ccoupon_remove = await AsyncStorage.removeItem('@coupon');
        
        this.setState({coupon_code: null});
        
    }
    refreshScreen() {
        this.setState({ lastRefresh: Date(Date.now()).toString() });
    }

    addOrder = async (discount, subtotal, save_amount) => {
            console.log('discount',discount);
            console.log('subtotal',subtotal);
            console.log('save_amount',save_amount);
            console.log('address',this.state.selectedaddress);
            console.log('cartItems',this.state.cartItems);
            console.log('coupon',this.state.coupon);
            console.log('order_information',this.state.order_information);

            let radioButtonsData = this.state.radioButtonsData;
            let payment_method = '';
            radioButtonsData.forEach(element => {
                if (element.selected) {
                    payment_method = element.label;
                   
                }
            });
            console.log('radioButtonsData',payment_method);
            let data = {
                payment_method      : payment_method,
                total               : discount,
                subtotal            : subtotal,
                save_amount         : save_amount,
                order_information   : this.state.order_information,
                selectAddress       : this.state.selectedaddress,
                CartList            : this.state.cartItems,
                used_coupon         : this.state.coupon,
            }


        let response = await add_order(data);
        // if(response.status){
        //     this.setState({coupons: response.data.data});
        // }
    }
    proceedTopay() {
            
    }

    async componentDidMount () {
        // await AsyncStorage.removeItem('@cart_items');
        let cartItems = await this.getCart();
        this.saveCart(cartItems);
        this.getCart();
        let {route} = this.props;
        
        let coupon = await AsyncStorage.getItem('@coupon'); 
        let coupon_code = await AsyncStorage.getItem('@coupon_code'); 
        coupon = coupon !== null ? JSON.parse(coupon) : {};
        
        coupon_code = coupon_code !== null ? JSON.parse(coupon_code) : null;
        
            
        this.setState({coupon: coupon});
        this.setState({coupon_code: coupon_code});


        let selectaddress = await AsyncStorage.getItem('@selectedAddress');
       
        selectaddress = selectaddress !== null ? JSON.parse(selectaddress) : null;
        this.setState({selectedaddress: selectaddress});
        // await AsyncStorage.removeItem('@selectedAddress');

        
    }
    onPressRadioButton(radioButtonsArray) {
        console.log('radop btns: ', radioButtonsArray);
        this.setState({radioButtonsData: radioButtonsArray});
    }
    
    
    
   
    
    render() {
        var products = this.state.products;
        // console.log('pro',products);
        // console.log('discount',this.state.discount);
        let {navigation, route} = this.props;
        var subtotal = 0;  
        var discount = 0;
        var save_amount = 0;
        var $input_arr = [];      
        var coupon_o = this.state.coupon;
        // console.log('coupon',this.state.coupon);
        const radioButtonsData = [{
            id: '1', // acts as primary key, should be unique and non-empty string
            label: 'Option 1',
            value: 'option1'
        }, {
            id: '2',
            label: 'Option 2',
            value: 'option2'
        }]

        setTimeout(() => {
            this.setState({ loading: true })
            }, 1000)
            if (this.state.loading) {
        return(
                
                    <View style={{backgroundColor:'#27ae60',headerMode: 'screen', display: 'flex', flexDirection: 'column', height: '100%'}}>
                        <Header {...this.props} />
                        <View style={[styles.main_content]}>
                            {
                            products && Object.values(products).length
                            ?
                            <ScrollView>
                                <View style={{padding:10}}>
                                    {
                                        products && Object.values(products).length 
                                        ? Object.values(products).map((product, index) => {
                                            let price = product.products_price * product.qty;
                                            subtotal += price;                                
                                            if( this.state.coupon.discount_type == 'percent'){
                                                if(this.state.coupon.minimum_amount <= subtotal){
                                                    save_amount = (this.state.coupon.amount / 100 ) * subtotal;
                                                    discount = subtotal - save_amount;
                                                } else {
                                                    discount = subtotal;
                                                }
                                                
                                            } else {
                                                if(this.state.coupon.minimum_amount <= subtotal){
                                                    discount = subtotal - this.state.coupon.amount;
                                                    save_amount = subtotal - discount;
                                                } else {
                                                    discount = subtotal;
                                                }
                                                
                                            }
                                            return (
                                                <Swipeout autoClose={true} right={[
                                                    {
                                                    component: (
                                                        <View
                                                            style={{
                                                                flex: 1,
                                                                alignItems: 'center',
                                                                justifyContent: 'center',
                                                                flexDirection: 'column',
                                                                backgroundColor:'red',
                                                                borderRadius:5,
                                                                marginBottom:10,
                                                                marginHorizontal:10
                                                            }}
                                                        >
                                                            <Icon
                                                                name='trash-outline'
                                                                type='ionicon'
                                                                color='#fff'
                                                                style={{ textAlign:"left" }}
                                                                />
                                                        </View>
                                                        ),
                                                        backgroundColor: 'transparent',
                                                        underlayColor: 'rgba(0, 0, 0, 0, 0)',
                                                        
                                                        onPress: () => {
                                                            this.removeProduct(product.id)
                                                        },
                                                    },
                                                ]}
                                                Close
                                                backgroundColor= 'transparent' key={index}>
                                                    <View style={{flexDirection:'row',backgroundColor:colors.white,borderRadius:5,overflow:'hidden',padding:10, marginBottom: 10}} >
                                                        
                                                        <View style={{flex:.2}}>
                                                            <Image style={styles.product_image} source={{uri: url + product.path}} />
                                                        </View>
                                                        <View style={{flex:.5,justifyContent:'center',paddingHorizontal:10,paddingLeft:20}}>
                                                            <Text style={styles.product_title}>{product.name}</Text>
                                                            <Text>₹{ product.products_price } x { product.qty } = <Text style={{fontWeight:'bold'}}>₹{price}</Text></Text>
                                                        </View>
                                                        <View style={{flex:.3,justifyContent:'center',paddingHorizontal:10}}>
                                                            <View style={{flexDirection:'row'}}>
                                                                <TouchableOpacity onPress={() => this.updateCart(product.id, 'minus')}>
                                                                    <View style={{flex:1,alignItems:'center'}}>
                                                                        <Icon type="antdesign" color={colors.white} name="minus" style={{ borderColor:colors.black, borderWidth:0,borderRadius:10,padding:3,textAlign:'center',backgroundColor:colors.primary }}/>
                                                                    </View>
                                                                </TouchableOpacity>
                                                                <View style={{flex:1,justifyContent:'center'}}>
                                                                    <Text style={{textAlign:'center',justifyContent:'center',fontWeight:'bold',fontSize:17}}>{product.qty}</Text>
                                                                </View>
                                                                <TouchableOpacity onPress={() => this.updateCart(product.id, 'plus')}>
                                                                    <View style={{flex:1}}>
                                                                        <Icon type="antdesign" color={colors.white} name="plus" style={{ borderColor:colors.black, borderWidth:0,borderRadius:10,padding:3,backgroundColor:colors.primary }}/>
                                                                    </View>
                                                                </TouchableOpacity>
                                                            </View>
                                                        </View>
                                                    </View>
                                                </Swipeout>
                                            );
                                        }) 
                                        : 
                                        <View style={{justifyContent:'center'}}>
                                            <Text style={{textAlign:'center',fontSize:20,paddingVertical:20}}>Cart Empty</Text>
                                        </View>
                                    }
                                    <>
                                    {
                                        !this.state.coupon_code
                                        ?
                                        <TouchableOpacity onPress={() => navigation.navigate('Coupon List')}>
                                            <View style={{flexDirection:'row'}}> 
                                                <View style={{flex:.8}}>                                    
                                                    <Text style={styles.coupon_input}>Promo Code</Text>
                                                </View>
                                                <View style={{flex:.4}}>
                                                    
                                                        <Text style={styles.coupon_apply}>
                                                            Apply
                                                        </Text>
                                                    
                                                    {/* <Button
                                                        title="Apply"
                                                        color={colors.primary}
                                                        buttonStyle={styles.coupon_apply}
                                                    /> */}
                                                </View>
                                            </View>
                                        </TouchableOpacity>
                                        :
                                        <View style={{flexDirection:'row'}}>                             
                                            <View style={{flex:1}}>                                    
                                                <Text style={styles.coupon_input}>{this.state.coupon.code}</Text>
                                            </View>
                                            <TouchableOpacity onPress={() => this.couponforgot()}>
                                                <View style={{flex:.2}}>
                                                    <Text style={styles.coupon_remove}>
                                                        <Icon
                                                        type="ionicon" 
                                                        color="red" 
                                                        name="close" 
                                                        />
                                                    </Text>
                                                </View>
                                            </TouchableOpacity>                                
                                        </View>
                                    }
                                    </>    
                                    <View style={{flexDirection:'row',marginTop:10,marginBottom:5,}}>
                                        <View style={{flex:.6}}>
                                            <Text style={styles.coupon_subtotal}>Subtotal</Text>                                    
                                        </View>
                                        <View style={{flex:.6}}>
                                            <Text style={[styles.coupon_subtotal_price,{fontSize:22}]}>₹{subtotal}</Text>
                                        </View>
                                    </View>
                                    
                                        {
                                            this.state.coupon_code
                                            ?
                                            <>
                                            <View style={{flexDirection:'row',marginVertical:5}}>
                                                <View style={{flex:.6}}>
                                                    <Text style={styles.coupon_subtotal}>Total Discount</Text>                                    
                                                </View>
                                                <View style={{flex:.6,justifyContent:'flex-end'}}>
                                                {   
                                                        this.state.coupon.discount_type == 'percent'
                                                        ?
                                                        <>
                                                        {
                                                            this.state.coupon.minimum_amount <= subtotal
                                                            ?
                                                            <Text style={styles.coupon_discount_price}>- ₹{save_amount}</Text>                                            
                                                            :
                                                            <Text style={[styles.coupon_discount_price,{color:"red",fontSize:14}]}>Your Coupon is not valid.</Text>
                                                        }
                                                        </>
                                                        :
                                                        <>
                                                        {
                                                            this.state.coupon.minimum_amount <= subtotal
                                                            ?
                                                            <Text style={styles.coupon_discount_price}>- ₹{save_amount}</Text>
                                                            :
                                                            <Text style={[styles.coupon_discount_price,{color:"red",fontSize:14}]}>Your Coupon is not valid.</Text>
                                                        }
                                                        </>
                                                    }
                                                    
                                                </View>
                                            </View>
                                            <View style={{flexDirection:'row',marginTop:5,marginBottom:10}}>
                                                <View style={{flex:.6}}>
                                                    <Text style={styles.coupon_total}>To Pay</Text>                                    
                                                </View>
                                                <View style={{flex:.6}}>
                                                    <Text style={styles.coupon_subtotal_price} >₹{discount}</Text>
                                                </View>
                                            </View>
                                            
                                            </> 
                                            :
                                            <></>                                
                                        }

                                        {/* <View style={{flexDirection:'row',marginVertical:5}}>
                                            <View style={{flex:.6}}>
                                                <Text style={styles.coupon_subtotal}>Save Amount</Text>                                    
                                            </View>
                                            <View style={{flex:.6}}>
                                                <Text style={styles.coupon_discount_price}>₹{discount}</Text>
                                            </View>
                                        </View> */}
                                        
                                        
                                        
                                        {/* <Button onPress={()  => this.refreshScreen()} title="Refresh Screen" /> */}
                                        
                                    </View>
                            </ScrollView>
                            :
                            <View style={{justifyContent:'center'}}>
                                <Text style={{textAlign:'center',fontSize:20,paddingVertical:20}}>Cart Empty</Text>
                            </View>
                            }
                            {
                            products && Object.values(products).length
                            ?
                            
                                <View style={{position:'absolute',bottom:0,right:0,left:0,borderTopWidth:1,borderTopColor:colors.darkgrey}}>

                                    {
                                        this.state.selectedaddress
                                        ?
                                        <View style={[styles.profile_menu_box,{marginVertical:0}]}>
                                            <View style={{flexDirection:'row',margin:10}}>
                                                <View style={{flex:.1,justifyContent:'center'}}>
                                                    
                                                        <Icon                                         
                                                        name='location-outline'
                                                        type='ionicon'
                                                        iconStyle={{fontSize:25,textAlign:'left'}}
                                                        color={colors.bgColor} />
                                                                                    
                                                </View>
                                                <View style={{flex:.7,justifyContent:'center'}}>
                                                    <Text style={styles.show_address_title}>{this.state.selectedaddress.house_no}, {this.state.selectedaddress.street}, {this.state.selectedaddress.landmark}, {this.state.selectedaddress.city}, {this.state.selectedaddress.postcode}</Text>                                    
                                                </View>
                                                <View style={{flex:.2,justifyContent:'center'}}>
                                                    <Text style={{fontSize:17,textAlign:'center',fontWeight:'bold',color:colors.primary}}>CHANGE</Text>
                                                </View>
                                            </View>
                                            {/* <TouchableOpacity onPress = {() => navigation.navigate('Address')}>
                                                <Text style={{backgroundColor:colors.darkgrey,color:colors.white,textAlign:'center',paddingVertical:10}}>CHANGE ADDRESS</Text>
                                            </TouchableOpacity> */}
                                        
                                        </View>
                                        :
                                        <TouchableOpacity onPress = {() => navigation.navigate('Address')}>
                                            <View style={{flexDirection:'row',backgroundColor:colors.white,borderRadius:5,overflow:'hidden',padding:10, marginVertical: 10}}>
                                                
                                                    <View style={styles.select_addrs_content}>
                                                        <View style={{flexDirection:'row',justifyContent:'center',paddingVertical:10}}>
                                                            <Icon
                                                                type="ionicon" 
                                                                color={colors.darkgrey} 
                                                                name="add" 
                                                                />
                                                            <View style={{justifyContent:'center'}}>
                                                                <Text style={styles.select_addrs_title}>SELECT ADDRESS</Text>
                                                            </View>
                                                            
                                                                
                                                        </View>
                                                    </View>
                                            </View>
                                        </TouchableOpacity>
                                    }
                                    <View style={{justifyContent:'flex-start',backgroundColor:colors.white,paddingHorizontal:10}}>
                                        <Input

                                            // style={[styles.cart_info]}
                                            placeholder="Write something..."
                                            leftIcon={{ type: 'MaterialIcon', name: 'description'}}
                                            numberOfLines={1}
                                            multiline={true}
                                            value={this.state.order_information}  
                                            onChangeText={order_information => this.setState({ order_information })}
                                        />
                                    </View>
                                    <View style={[styles.pay_method_content]}>
                                        <View style={{justifyContent:'center'}}>
                                            <Text style={{fontSize:20}}>Payment Method :</Text>
                                        </View>
                                        <View>
                                        <RadioGroup 
                                            radioButtons={this.state.radioButtonsData} 
                                            onPress={() => this.onPressRadioButton}
                                            layout='row'
                                        />
                                        </View>
                                    </View>

                                    {/* <TouchableOpacity>
                                        <Text style={styles.checkout_btn}>
                                            Procced To Checkout
                                        </Text>
                                    </TouchableOpacity> */}
                                    <View style={[styles.cart_footer_btn,{ flexDirection:'row' }]}>
                                        <View style={{flex:.6}}>
                                            <View style={styles.last_total_pay}>
                                                <Text style={styles.last_total_price}>₹{discount}</Text>
                                            </View>                                
                                        </View>
                                        
                                        <View style={{flex:.6}}>
                                            {
                                                this.state.coupon_code
                                                ?
                                                <TouchableOpacity  onPress={() => this.addOrder(discount, subtotal, save_amount)}>
                                                    <Text style={styles.checkout_btn}>
                                                    PROCEED TO PAY
                                                    </Text>
                                                </TouchableOpacity>
                                                :
                                                <TouchableOpacity  onPress={() => this.addOrder(subtotal)}>
                                                    <Text style={styles.checkout_btn}>
                                                    PROCEED TO PAY
                                                    </Text>
                                                </TouchableOpacity>
                                            }
                                            
                                        </View>
                                    </View>
                                </View>
                                :
                                <></>
                            }
                            
                        
                    </View>
                </View>
        );
    } else {
        return (
          <View style={{flex : 1, justifyContent: 'center', alignItems: 'center',}}>                            
            <ActivityIndicator  color={colors.primary} />
          </View>
          );
      }
    }
}