import React, { Component } from "react"
import { SafeAreaView, ScrollView, View, Text, ActivityIndicator } from "react-native";
import { Icon } from "react-native-elements";
import { get_order } from "../../Api/order.api";
import Header from "../../components/common/Header";
import { colors } from "../../configs/colors.config";
import { styles } from "./styles";
import Moment from 'moment';

export default class OrderScreen extends Component{
    state = {
        orders:[],
    }
    getOrder = async () => {
    


    let response = await get_order();
    if(response.status){
        this.setState({orders: response.data.data});
    }

    
    }
    async componentDidMount () {
        this.getOrder();
    }
    render() {
        
        console.log('orders',this.state.orders.order_product);
        let {navigation} = this.props;
        setTimeout(() => {
            this.setState({ loading: true })
          }, 1000)
          if (this.state.loading) {
        return(
                
                    <View style={{backgroundColor:'#27ae60',headerMode: 'screen', display: 'flex', flexDirection: 'column', height: '100%'}}>
                        <Header {...this.props} />
                        <View style={[styles.main_content]}>
                        <ScrollView>
                            <View style={{padding:10}}>
                            

                            {
                                this.state.orders.map((order, index) => {
                                   
                                    return(
                                        <View style={styles.order_history_content}>
                                            <View style={{flexDirection:'row'}}>
                                                <View style={{flex:.6}}>
                                                    <Text style={styles.order_id}>Order #{order.orders_id}</Text>
                                                    <View style={{flexDirection:'row'}}>
                                                        {/* <View style={{flex:.2}}>
                                                            <Icon 
                                                            name='local-shipping'
                                                            type='MaterialIcons'
                                                            color='#a1a1a1'
                                                            style={{ textAlign:"left" }}
                                                            /> 
                                                        </View> */}
                                                        <View style={{flex:.8}}>
                                                            {/* <Text style={{color:colors.darkgrey}}>Shipped</Text> */}
                                                        </View>
                                                    </View>                        
                                                </View>
                                                <View style={{flex:.6}}>
                                                    <Text style={styles.order_date}>{ Moment(order.created_at).format('MMM d, YYYY') }</Text>
                                                </View>
                                            </View>
                                            {
                                                
                                                order.order_product.map((product, index) => {
                                                 return(
                                                    <View style={{flexDirection:'row',borderBottomColor:colors.grey,borderBottomWidth:1,paddingVertical:10}}>
                                                        <View style={{flex:.6}}>
                                                            <Text style={styles.product_name}>{product.products_name}</Text>                        
                                                        </View>
                                                        <View style={{flex:.2}}>
                                                            <Text style={styles.product_price}>qty {product.products_quantity}</Text>
                                                        </View>
                                                        <View style={{flex:.4}}>
                                                            <Text style={styles.product_price}>₹{product.products_price}</Text>
                                                        </View>
                                                    </View>
                                                 );
                                             })
                                            }
                                            
                                            <View style={{flexDirection:'row',paddingVertical:10,}}>
                                                <View style={{flex:.7}}>
                                                    <View style={{flexDirection:'row'}}>
                                                        <View style={{flex:.2}}>
                                                            <Icon 
                                                            name='location-outline'
                                                            type='ionicon'
                                                            color='#a1a1a1'
                                                            style={{ textAlign:"left" }}
                                                            /> 
                                                        </View>
                                                        <View style={{flex:.8}}>
                                                            <Text style={{color:colors.darkgrey}}>{order.billing_street_address}, {order.billing_city},{order.billing_state}, {order.billing_postcode}</Text>
                                                        </View>
                                                    </View>                        
                                                </View>
                                                <View style={{flex:.2}}>
                                                    <Text style={styles.product_price}>Total</Text>
                                                </View>
                                                <View style={{flex:.3}}>
                                                    <Text style={[styles.product_price,{fontSize:20,color:colors.black}]}>₹{order.order_price}</Text>
                                                </View>
                                            </View>
                                        </View>
                                    );
                                })
                            }
                            
                            </View>
                        </ScrollView>
                        </View>
                        
                    </View>
        );
    } else {
        return (
          <View style={{flex : 1, justifyContent: 'center', alignItems: 'center',}}>                            
            <ActivityIndicator  color={colors.primary} />
          </View>
          );
      }
    }
}