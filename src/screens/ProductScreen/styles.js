import { StyleSheet } from "react-native";
import { colors } from "../../configs/colors.config";

export const styles = StyleSheet.create({
    scrollView: {

    },
    navigation_title: {
        color:colors.white,
        fontSize: 25,
        marginVertical:18,
        justifyContent:'center',
        
      },
      container: {
        flex: 1,
        flexDirection: "column"
      },
      
      main_content: {
        backgroundColor:'#f9f9f9',
        // height:'100%',
        paddingBottom: 60,
        borderTopLeftRadius:30,
        borderTopRightRadius:30,
        overflow: 'hidden',
        
        // padding:10,
        // marginBottom:85,
        flexGrow: 1,
        // paddingBottom:40
    },   
      product_img_content: {
        // borderWidth:1,
        borderRadius:15,
        overflow: 'hidden',
        paddingBottom:0
      },
      product_image: {
        width: '100%',
        resizeMode:'contain',
        borderRadius:15
      },
      home_products: {
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'center',
        marginHorizontal: -15,
    },
    product_space: {
        width: '50%',
        paddingHorizontal: 15,
        marginBottom: 15
    },
    wrapper: {
      height:300
    },
    slide1: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
    slide2: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
    slide3: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
    text: {
      color: '#fff',
      fontSize: 30,
      fontWeight: 'bold',
    },
    product_title: {
      marginTop:15,
      // height:200,
      // color:'black', 
      fontSize:30,
      fontWeight:'bold'
    },
    product_desc: {
      marginTop:10,
      // height:200,
      // color:'black', 
      fontSize:17,
    },
    product_price: {
      // marginTop:15,
      fontSize:29,
      fontWeight: 'bold',
      backgroundColor: colors.primary,
      borderTopRightRadius:50,
      borderBottomRightRadius:50,
      padding:5,
      paddingVertical:14,
      // minWidth:100,
      maxWidth:160,
      color:colors.white,
      flexDirection:'row',
      alignItems:'baseline',
      justifyContent:'center',
      // height:50
    },
    product_price_amount : {
      color:colors.white,
      fontSize:21,
      fontWeight:'bold'
      
    },
    product_unit : {
      color:colors.white,
      fontSize:15,

    },
    cart_box: {
      position: 'absolute',
      bottom: 5,
      right: 5
    },
    add_cart_btn: {
      backgroundColor: colors.primary,
      // alignItems:'center',
      borderRadius: 15,
      width: 30,
      height: 30,
      justifyContent:'center'
    },
    similar_product_title: {
      fontSize:22,
      fontWeight:'bold',
      paddingVertical:20
    },
    cat_header_left: {
      fontSize:20,
      fontWeight:'bold',
      color: colors.black,
    },
    cat_header_right: {
      color:'#3bb56e',
      fontSize:16,
      fontWeight:'bold'
    },
    home_products: {
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        // justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: -15
    },
    product_space: {
        width: '50%',
        paddingHorizontal: 15,
        marginBottom: 15
    },
    product_content: {
      // backgroundColor: colors.white,
      borderRadius:25,
      // padding:15,
      paddingVertical:15,
      // marginBottom:80
      // paddingBottom:40
      
    },
});