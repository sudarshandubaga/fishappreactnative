import React, { Component } from "react";
import { View, Image, StyleSheet, Text, TouchableOpacity, ActivityIndicator } from "react-native";
import { Icon } from "react-native-elements";
import { ScrollView } from "react-native-gesture-handler";
import { SafeAreaView } from "react-native-safe-area-context";
import { getcategoryToproducts, getpopularproducts } from "../../Api/product.api";
import Bottomnavigation from "../../components/Bottomnavigation";
import Header from "../../components/common/Header";
import ProductBox from "../../components/ProductBox";
import { colors } from "../../configs/colors.config";
import { styles } from "./styles";

export default class ProductsScreen extends Component {
    state = {
      products: [],
      cartItems: {}
    }

    getcategoryToproducts = async () => {
      let {route} = this.props;
        // console.log('aad',route.params);
      let response = await getcategoryToproducts(route.params);
      // console.log('res', response);
      if(response.status){
          this.setState({products: response.data});
          
      }
    }
    getpopularproducts = async () => {
      let response = await getpopularproducts(); 
      // console.log('ab',response.data);   
      if(response.status) {
        this.setState({products: response.data});
        
      }
      
      
    }


    async componentDidMount () {
        this.getcategoryToproducts();
        this.getpopularproducts();
    }
    addToCart = (p, id) => {
        let cartItems = this.state.cartItems;
        p.qty = 1;
        cartItems[id] = p;
       this.setState(cartItems);
      }
   
      updateCart = (id, type) => {
       let cartItems = this.state.cartItems;
   
       let qty = cartItems[id].qty;
       qty = parseInt(qty);
   
       if(type === 'plus') {
         qty += 1;
       } else {
         qty -= 1;
       }
   
       if(qty > 0)
         cartItems[id].qty = qty;
       else
         delete cartItems[id]
   
       this.setState(cartItems);
      }
    render() {
        let {navigation, route} = this.props;
        setTimeout(() => {
          this.setState({ loading: true })
        }, 3000)
        if (this.state.loading) {
        return (      
          <View style={{backgroundColor:colors.primary,headerMode: 'screen',height:'100%',}}>
            <Header {...this.props} />
            <View style={styles.main_content}>
                      
            <ScrollView style={styles.scrollView}>
              <View style={{padding:10}}>
                <View style={styles.product_content}>                            
                    <View style={styles.home_products}>
                        {
                          this.state.products.map((product, index) => {
                            return (
                              <View style={styles.product_space} key={index}>
                                  <TouchableOpacity onPress={() => navigation.navigate('Product',{detail:product})}>
                                    <ProductBox 
                                        {...this.props}
                                        data={product} 
                                        id={index} 
                                        cartItems={this.state.cartItems} 
                                        addToCart={(p, id) => this.addToCart(p,id)} 
                                        updateCart={(id, type) => this.updateCart(id,type)}
                                    />
                                  </TouchableOpacity>
                                </View>
                            );
                          })
                        }
                    </View>
                    </View>
                  </View>
              </ScrollView>
            </View>
          </View>
        );
        
      } else {
        return (
          <View style={{flex : 1, justifyContent: 'center', alignItems: 'center',}}>                            
            <ActivityIndicator  color={colors.primary} />
          </View>
          );
      }
    }
}