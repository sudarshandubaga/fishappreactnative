import React, { Component, useRef, useState } from "react";
import { SafeAreaView, ScrollView, Text, View, Image, FlatList, TouchableOpacity, Dimensions, ActivityIndicator } from "react-native";
// import { Icon } from "react-native-elements/dist/icons/Icon";
import { Icon } from 'react-native-elements';
import Slick from "react-native-slick";
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { Colors } from "react-native/Libraries/NewAppScreen";
import { getpopularproducts, getproductdetail } from "../../Api/product.api";
import Header from "../../components/common/Header";
import ProductBox from "../../components/ProductBox";
import { colors } from "../../configs/colors.config";
import { styles } from "./styles";

export default class ProductScreen extends Component {
    
    state = {
        indexSelected: 0,
        images: [
            {
                id: 1,
                image: require('../../../imgs/mahadev.jpg')
            },
            {
                id: 2,
                image: require('../../../imgs/mahadev.jpg')
            },
            {
                id: 3,
                image: require('../../../imgs/mahadev.jpg')
            },
            {
                id: 4,
                image: require('../../../imgs/mahadev.jpg')
            },
            {
                id: 5,
                image: require('../../../imgs/mahadev.jpg')
            },
            {
                id: 6,
                image: require('../../../imgs/mahadev.jpg')
            }
        ],
        cartItems: {},
        
        popularproducts: [],
        productdetail: [],
        similarproduct: [],
    }

    getproductdetail = async () => {
        let {route} = this.props;
        let product_id = route.params.detail.id;
        let response = await getproductdetail(product_id);
        // console.log('res',response);
        if(response.status){
            
            this.setState({productdetail: response.data.data});            
            this.setState({similarproduct: response.data.similarProducts});            
            
        }
    }

    getpopularproducts = async () => {
    
        let response = await getpopularproducts(); 
        // console.log('ab',response.data);   
        if(response.status) {
          this.setState({popularproducts: response.data});
          
        }
      }

    async componentDidMount () {
        this.getproductdetail();
        this.getpopularproducts();
    }


    constructor(props) {
        super(props);
        this.carouselRef = React.createRef();
    }
    
    onTouchThumbnail = touched => {
        if (touched === this.state.indexSelected) return;
        
        this.carouselRef?.current?.snapToItem(touched);
    }
    onSelect = indexSelected => {
        this.setState({indexSelected});
    }
    addToCart = (p, id) => {
        let cartItems = this.state.cartItems;
        p.qty = 1;
        cartItems[id] = p;
       this.setState(cartItems);
      }
   
      updateCart = (id, type) => {
       let cartItems = this.state.cartItems;
   
       let qty = cartItems[id].qty;
       qty = parseInt(qty);
   
       if(type === 'plus') {
         qty += 1;
       } else {
         qty -= 1;
       }
   
       if(qty > 0)
         cartItems[id].qty = qty;
       else
         delete cartItems[id]
   
       this.setState(cartItems);
      }
    addToCart = () => {
        this.props.addToCart(this.props.data, this.props.id)
      }
      updateCart = (type) => {
        if(this.props.cartItems[this.props.id]) {
          this.props.updateCart(this.props.id, type);
        }
      }

    render() {
        const { width } = Dimensions.get('window');
        const settings = {
            autoplay:true,
            loop:false,
            showsButtons:true,
            height:200,
            flex:1,
        }
        
        let {navigation, route} = this.props;
        // const { itemId, otherParam } = route.params;
        let producte = route.params.detail;
        // console.log('product',producte);
        setTimeout(() => {
            this.setState({ loading: true })
          }, 1000)
          if (this.state.loading) {
        return (
            
                    <View style={{backgroundColor:'#27ae60',headerMode: 'screen',height:'100%'}}>
                        <Header {...this.props} />
                        <View style={styles.main_content}>
                                    <ScrollView>
                                    <View style={{padding:10}}>
                                        
                                        <View style={styles.product_img_content}>
                                            
                                            {/* <Slick style={styles.wrapper} showsButtons={true} {...settings}>
                                                <View style={styles.slide1}>
                                                <Image style={styles.product_image} source={require('../../../imgs/mahadev.jpg')} />
                                                </View>
                                                <View style={styles.slide2}>
                                                <Image style={styles.product_image} source={require('../../../imgs/logo.png')} />
                                                </View>
                                                <View style={styles.slide3}>
                                                <Image style={styles.product_image} source={require('../../../imgs/skipjack-tuna.png')} />
                                                </View>
                                            </Slick> */}
                                            <View style={{ height: 325 }}>
                                                <View style={{flex: 1/2, marginTop: 20}}>
                                                    <Carousel
                                                        ref={this.carouselRef}
                                                        layout='default'
                                                        data={this.state.images}
                                                        sliderWidth={width -20}
                                                        itemWidth={width -20}
                                                        renderItem={({ item, index }) => (
                                                            <Image
                                                                key={index}
                                                                style={{ width: '100%', height: '100%',borderRadius:15 }}
                                                                resizeMode='cover'
                                                                source={item.image}
                                                            />
                                                        )}
                                                        onSnapToItem={index => this.onSelect(index)}
                                                    />
                                                    {/* <Pagination
                                                        inactiveDotColor='gray'
                                                        dotColor={'orange'}
                                                        activeDotIndex={this.state.indexSelected}
                                                        dotsLength={this.state.images.length}
                                                        animatedDuration={150}
                                                        inactiveDotScale={1}
                                                    /> */}
                                                </View>
                                                
                                                <FlatList
                                                    horizontal={true}
                                                    data={this.state.images}
                                                    style={{ position: 'absolute', bottom: '10%' }}
                                                    showsHorizontalScrollIndicator={false}
                                                    contentContainerStyle={{
                                                        paddingHorizontal: 0,
                                                        
                                                    }}
                                                    keyExtractor={item => item.id}
                                                    renderItem={({ item, index }) => (
                                                        <TouchableOpacity 
                                                            activeOpacity={0.9}
                                                            onPress={() => this.onTouchThumbnail(index)}
                                                        >
                                                        <Image
                                                            style={{
                                                                width: 70,
                                                                height: 70,
                                                                marginRight: 10,
                                                                borderRadius: 16,
                                                                borderWidth: index === this.state.indexSelected ? 4 : 0.75,
                                                                borderColor: index === this.state.indexSelected ? 'orange' : 'white'
                                                            }}
                                                            source={item.image}
                                                        />
                                                        </TouchableOpacity>
                                                    )}
                                                />
                                            </View>
                                        </View>
                                        <View>
                                            <Text style={styles.product_title}>{this.state.productdetail.name}</Text>
                                            <Text style={styles.product_desc}>{this.state.productdetail.products_description}</Text>
                                            
                                            
                                            
                                        </View>
                                
                                {/* <View style={styles.cart_box}>
                                    {
                                    this.props.cartItems && this.props.cartItems[this.props.id]
                                        ?
                                        <View style={{ backgroundColor:colors.secondary,borderRadius:50,justifyContent:'center',width:30 }}>
                                        <TouchableOpacity onPress={() => this.updateCart('minus')}>
                                            <View style={styles.add_cart_btn}>
                                            <Icon type="antdesign" color={colors.white} name="minus" />
                                            </View>
                                        </TouchableOpacity>
                                        <View style={{alignItems:'center',width:30,height:20,justifyContent:'center',color:colors.white}}>
                                            <Text style={{color:colors.white}}>{ this.props.cartItems[this.props.id].qty }</Text>
                                        </View>
                                        <TouchableOpacity onPress={() => this.updateCart('plus')}>
                                            <View style={styles.add_cart_btn}>
                                            <Icon type="antdesign" color={colors.white} name="plus" />
                                            </View>
                                        </TouchableOpacity>
                                        </View>
                                        :
                                    <TouchableOpacity onPress={() => this.addToCart()}>
                                        <View style={styles.add_cart_btn}>
                                        <Icon type="antdesign" color={colors.white} name="plus" />
                                        </View>
                                    </TouchableOpacity>
                                    }
                                </View> */}
                                    </View>
                                    <View style={{flexDirection:'row'}}>
                                        <View style={{flex:1}}>
                                            <View style={styles.product_price}>
                                                <Text style={styles.product_price_amount}>{ this.state.productdetail.products_price }</Text>
                                                <Text style={styles.product_unit}> ( { this.state.productdetail.products_weight }{ this.state.productdetail.products_weight_unit } ) </Text>
                                                
                                            </View>
                                        </View>
                                        <View style={{flex:1,justifyContent:'flex-end',alignItems:'flex-end',}}>
                                            <View style={{backgroundColor:colors.primary,width:160,borderTopLeftRadius:50,borderBottomLeftRadius:50,justifyContent:'center',padding:5,alignItems:'center',paddingVertical:14}}>
                                                <Text style={{color:colors.white,fontSize:20,fontWeight:'bold'}}>ADD TO CART</Text>
                                            </View>
                                        </View>
                                    </View>
                                    
                                    <View style={{padding:10}}>
                                        <View style={styles.product_content}>
                                        {
                                        this.state.similarproduct
                                        ?
                                            <View style={{ flexDirection: "row",borderBottomWidth:1,borderBottomColor:'#d5d5d5',paddingBottom:15,marginBottom:15}}>
                                                <View style={{ flex: 1}}>
                                                    <View>
                                                        <Text style={styles.cat_header_left}>Similar Product</Text>
                                                    </View>
                                                </View>
                                                <View >
                                                    <TouchableOpacity  onPress={() => navigation.navigate('Products')}>
                                                        <Text style={styles.cat_header_right}>View All</Text>
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                            :
                                            <></>
                                    }
                                            <View style={styles.home_products}>
                                                {
                                                    this.state.similarproduct.map((product, index) => {
                                                    return (
                                                        <View style={styles.product_space} key={product.id}>
                                                        <TouchableOpacity onPress={() => navigation.navigate('Product', {detail:product})}>
                                                            <ProductBox 
                                                            {...this.props}
                                                            data={product} 
                                                            id={index} 
                                                            cartItems={this.state.cartItems} 
                                                            addToCart={(p, id) => this.addToCart(p,id)} 
                                                            updateCart={(id, type) => this.updateCart(id,type)}
                                                            />
                                                        </TouchableOpacity> 
                                                        </View>
                                                    );
                                                    })
                                                }
                                            </View>
                                        </View>
                                    </View>
                                </ScrollView>
                                
                            
                        </View>
                        
                    </View>
        );
    } else {
        return (
          <View style={{flex : 1, justifyContent: 'center', alignItems: 'center',}}>                            
            <ActivityIndicator  color={colors.primary} />
          </View>
          );
      }
    }
}