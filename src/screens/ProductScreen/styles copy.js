import { StyleSheet } from "react-native";
import { colors } from "../../configs/colors.config";

export const styles = StyleSheet.create({
    scrollView: {

    },
    main_content: {
        backgroundColor:'#f9f9f9',
        // height:'100%',
        paddingBottom: 60,
        borderTopLeftRadius:30,
        borderTopRightRadius:30,
        overflow: 'hidden',
        
        // padding:10,
        // marginBottom:85,
        flexGrow: 1,
        // paddingBottom:40
    },    
    product_title: {
        fontSize:17,
        fontWeight:'bold'
    },
    profile_pic_content: {
        marginVertical:40,
        justifyContent:'center',
        alignItems:'center',
    },
    profile_pic_brdr: {
        borderColor:colors.bgColor, 
        padding:3,
        borderWidth:3,
        // border-style: dotted solid double
        // borderTopColor:'transparent',
        borderRadius:50,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    product_image: {
        width:80,
        height:80,
        borderRadius:50,
    },
    profile_name: {
        fontSize: 22,
        color: colors.white,
        marginTop:20
    },
    profile_menu_box: {
        marginVertical:5,
        backgroundColor: colors.white,
        borderRadius:5,
        padding:10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,

        elevation: 3,
    },
    profile_point_title:{
        fontSize:19,
        color:colors.primary
    },
    show_address_title: {
        fontSize:19,
        // color:colors.darkgrey
    },
    acc_left_name: {
        fontSize:17
    },
    acc_right_name: {
        fontSize:19
    },
    edit_profile: {
        marginVertical:10,
        backgroundColor:colors.primary,
        borderRadius:5,
        alignItems:'flex-end',
        // width:30,
        padding:5,
        justifyContent:'flex-end',
        
    }
});