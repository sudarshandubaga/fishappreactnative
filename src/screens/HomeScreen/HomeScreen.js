/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

 import React, { Component } from 'react';
 import Bottomnavigation from '../../components/Bottomnavigation';

 import {
   SafeAreaView,
   ScrollView,
   StatusBar,
   Text,
   View,  
   TextInput,
   TouchableOpacity,
   Image,
   Dimensions,
 } from 'react-native';
import { styles } from './styles';
import ProductBox from '../../components/ProductBox';
import CategoryBox from '../../components/CategoryBox';
import Slide from '../../components/Slide';
import { colors } from '../../configs/colors.config';
import Slick from 'react-native-slick';
import { getpopularproducts, get_products } from '../../Api/product.api';
import { get_main_categories } from '../../Api/category.api';
import { getbanners } from '../../Api/banner.api';
import { url } from '../../configs/constants.config';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import AsyncStorage from '@react-native-async-storage/async-storage';

 
 // import Carousel from 'react-native-snap-carousel';
 
 export default class HomeScreen extends Component {
   state = {
    products: [
      { 
        id: 1,
        title: "Blue Marlin",
        price: "₹499",
        image: require('../../../imgs/mahadev.jpg')
      },
      { 
        id: 2,
        title: "Socky Selmon",
        price: "₹499",
        image: require('../../../imgs/socky-selmon.png')
      },
      { 
        id: 3,
        title: "Skipjack Tuna",
        price: "₹499",
        image: require('../../../imgs/skipjack-tuna.png')
      },
      { 
        id: 4,
        title: "Gilt Head Bream",
        price: "₹499",
        image: require('../../../imgs/gilt-head-bream.png')
      },
      { 
        id: 5,
        title: "Blue Marlin",
        price: "₹499",
        image: require('../../../imgs/blue-marlin.png')
      },
      { 
        id: 6,
        title: "Blue Marlin",
        price: "₹499",
        image: require('../../../imgs/blue-marlin.png')
      }
    ],
    user: null,
    token: null,
    loading: false,
    categories: [],
    banners:[],
    popularproducts:[],
    cartItems: '',
    indexSelected: 0,
        images: [
            {
                id: 1,
                image: require('../../../imgs/mahadev.jpg')
            },
            {
                id: 2,
                image: require('../../../imgs/mahadev.jpg')
            },
            {
                id: 3,
                image: require('../../../imgs/mahadev.jpg')
            },
            {
                id: 4,
                image: require('../../../imgs/mahadev.jpg')
            },
            {
                id: 5,
                image: require('../../../imgs/mahadev.jpg')
            },
            {
                id: 6,
                image: require('../../../imgs/mahadev.jpg')
            }
        ],
      lastRefresh: Date(Date.now()).toString(),
    
   }
   

  //  addToCart = (p, id) => {
  //   let cartItems = this.state.cartItems;
  //   p.qty = 1;
  //   cartItems[id] = p;
  //   this.setState(cartItems);
  //   // SyncStorage
  //  }

  //  updateCart = (id, type) => {
  //   let cartItems = this.state.cartItems;

  //   let qty = cartItems[id].qty;
  //   qty = parseInt(qty);

  //   if(type === 'plus') {
  //     qty += 1;
  //   } else {
  //     qty -= 1;
  //   }

  //   if(qty > 0)
  //     cartItems[id].qty = qty;
  //   else
  //     delete cartItems[id]

  //   this.setState(cartItems);
  //  }

  getProducts = async () => {
    
    let response = await get_products();
    if(response.status) {
      this.setState({products: response.data.product_data});
    }
  }

  getpopularproducts = async () => {
    
    let response = await getpopularproducts(); 
    // console.log('ab',response.data);   
    if(response.status) {
      this.setState({popularproducts: response.data});
      
    }
    
    
  }

  refreshScreen() {
    this.setState({ lastRefresh: Date(Date.now()).toString() });
}

  getMainCategories = async () => {
    let response = await get_main_categories();
     if(response.status) {
       this.setState({categories: response.data});
      }
  }
  getBanners = async () => {
    let response = await getbanners();
    // let token = await AsyncStorage.getItem('@token');
    // let user = await AsyncStorage.getItem('@user');
    // console.log('user',user.user_name);
    // console.log('token',token);
    if(response.status) {
      this.setState({banners: response.data.data});
      
    }
  }
  

   async componentDidMount () {
    
     this.getProducts();
     this.getMainCategories();
     this.getBanners();
     this.getpopularproducts();

     let cartItems = await AsyncStorage.getItem('@cart_items');
      cartItems = cartItems !== null ? JSON.parse(cartItems) : {};
      this.setState({cartItems: cartItems});

      

     let user = await AsyncStorage.getItem('@user');
    let token = await AsyncStorage.getItem('@token');

    // console.log('token',user);

    if (user && token) {
      user = JSON.parse(user);
      this.setState({
        user: user,
        token: token
      });
    }
   }

  

   render() {
    // console.log('popularproducts' , this.state.popularproducts);
    const { width } = Dimensions.get('window');
    const settings = {
        autoplay:true,
        loop:false,
        showsButtons:true,
        height:200,
        flex:1,
    }
    setTimeout(() => {
      this.setState({ loading: true })
    }, 3000)
    
     let {navigation} = this.props;
     if (this.state.loading) {
     return (
      
   <SafeAreaView  >
     <StatusBar barStyle='light-content' backgroundColor={colors.primary}/>
     <View style={{backgroundColor:colors.primary,headerMode: 'screen',height:'100%',}}>
       <View style={{marginHorizontal: 15}}>
         {/* <View style={{ flexDirection: "row"}} >      
           <View style={{ flex: 1}}>
             <View style={styles.header_text}>
               <Text style={styles.header_text}>Hey,</Text>
               <Text style={styles.header_text}>Lets search your fish.</Text>
             </View>
           </View>
           <View >
             <View></View>
             <Image style={styles.tinyLogo} source={require('../../../imgs/logo.png')} />
           </View>
         </View> */}
         <View>
         {/* <TextInput
         style={styles.search_input}
           placeholder="Search fishes..."
         /> */}
         <TouchableOpacity onPress={() => navigation.navigate('Search Product')}>
          <Text style={styles.search_input_btn}> Search fishes...</Text>
         </TouchableOpacity>
         </View>
       </View>
       <ScrollView style={styles.scrollView}>
       <View style={styles.main_content}>
         
         <View style={styles.cat_content}>
           <View style={{ flexDirection: "row",borderBottomWidth:1,borderBottomColor:'#d5d5d5',paddingBottom:15,marginBottom:15}}>
             <View style={{ flex: 1}}>
               <View>
                 <Text style={styles.cat_header_left}>Categories</Text>
               </View>
             </View>
             <View >

               <TouchableOpacity  onPress={() => navigation.navigate('Category')}>
                  <Text style={styles.cat_header_right}>View All</Text>
               </TouchableOpacity>
             </View>
           </View>
           <View style={styles.home_categories}>
             {
                this.state.categories.map((category, index) => {
                  // console.log('id', category.id);
                  return (
                    <View style={styles.category_space} key={category.id}>
                      {
                        
                        <TouchableOpacity  onPress={() => category.parent_count ? navigation.navigate('Category',category.id) : navigation.navigate('Products',category.id)}>
                          <CategoryBox data={category} />
                        </TouchableOpacity>
                      } 
                      </View>
                    
                  );
                })
              }
           </View>
         </View>
         {/* <View style={styles.offer_content}>
              <Slick style={styles.wrapper} showsButtons={true} {...settings}>
                { 
                  this.state.banners && this.state.banners.map((banner, index) => {
                    console.log('image url: ',banner);
                    return (
                      <View style={styles.slide} key={index}>
                        <Image style={styles.product_image} source={{uri: url + banner.image}} />
                    </View>
                    );
                  })
                  
                }
                  
              </Slick>
         </View> */}
         <View style={{ height: 150 }}>
            <View style={{flex: 1, marginTop: 20}}>
                <Carousel
                    ref={this.carouselRef}
                    layout='default'
                    data={this.state.banners}
                    sliderWidth={width -20}
                    itemWidth={width -20}
                    renderItem={({ item, index }) => (
                      
                        <Image
                            key={index}
                            style={{ width: '100%', height: '100%',borderRadius:15 }}
                            resizeMode='cover'
                            source={{uri: url + item.image}}
                        />
                    )}
                   
                />
                {/* <Pagination
                    inactiveDotColor='gray'
                    dotColor={'orange'}
                    activeDotIndex={this.state.indexSelected}
                    dotsLength={this.state.banners.length}
                    animatedDuration={150}
                    inactiveDotScale={1}
                /> */}
            </View>
          </View>
         <View style={styles.product_content}>
           <View style={{ flexDirection: "row",borderBottomWidth:1,borderBottomColor:'#d5d5d5',paddingBottom:15,marginBottom:15}}>
             <View style={{ flex: 1}}>
               <View>
                 <Text style={styles.cat_header_left}>Similar Products</Text>
               </View>
             </View>
             <View >
               <TouchableOpacity  onPress={() => navigation.navigate('Products')}>
                  <Text style={styles.cat_header_right}>View All</Text>
               </TouchableOpacity>
             </View>
           </View>
           
           <View style={styles.home_products}>
              {
                this.state.popularproducts.map((product, index) => {
                  return (
                    <View style={styles.product_space} key={product.id}>
                      <TouchableOpacity onPress={() => navigation.navigate('Product', {detail:product})}>
                        <ProductBox 
                          {...this.props}
                          data={product} 
                          id={index} 
                          cartItems={this.state.cartItems} 
                          addToCart={(p, id) => this.addToCart(p,id)} 
                          updateCart={(id, type) => this.updateCart(id,type)}
                        />
                      </TouchableOpacity> 
                    </View>
                  );
                })
              }
           </View>
         </View>
       </View>
       </ScrollView>
     </View>
     
     
     <Bottomnavigation {...this.props} />
     
   </SafeAreaView>
     );
    } else {
      return (
        <View style={{flex:1,backgroundColor:colors.bgColor,justifyContent:'center'}}>
          <StatusBar barStyle='light-content' backgroundColor={colors.bgColor}/>
          <View>
            <Text style={{ color: 'black',textAlign:'center',fontSize:22 }}>Loading...</Text>
          </View>
        </View>
      )
    }
   }
 }
 