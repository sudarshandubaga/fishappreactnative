import { StyleSheet } from "react-native";
import { colors } from "../../configs/colors.config";

export const styles = StyleSheet.create({
    scrollView: {
      // backgroundColor: 'pink',
    },
    wrapper: {
      width: '100%',
      minHeight: '100%',
      backgroundColor: '#3498db',
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
      color: 'white',
    },
    container: {
      flex: 1,
      flexDirection: "column"
    },
    search_input:{
      backgroundColor:colors.white,
      borderRadius:5,
      paddingHorizontal:15,
      marginVertical:15
    },
    search_input_btn:{
      backgroundColor:colors.white,
      borderRadius:5,
      paddingHorizontal:15,
      paddingVertical:15,
      marginVertical:15,
      color:colors.darkgrey,
    },
    header_text: {
      color:colors.white,
      fontSize:20,
    },
    main_content: {
      backgroundColor:'#f9f9f9',
      height:'100%',
      borderTopLeftRadius:30,
      borderTopRightRadius:30,
      padding:10
    },
    cat_content: {
      backgroundColor: colors.white,
      borderRadius:25,
      padding:15,
      // marginBottom:80
      // paddingBottom:40
      
    },
    
    cat_header_left: {
      fontSize:20,
      fontWeight:'bold',
      color: colors.black,
    },
    cat_header_right: {
      color:'#3bb56e',
      fontSize:16,
      fontWeight:'bold'
    },
    product_content: {
      backgroundColor: colors.white,
      borderRadius:25,
      padding:15,
      marginBottom:80
      // paddingBottom:40
      
    },
    
    offer_content: {
      backgroundColor: colors.bgColor,
      borderRadius:25,
      height:150,
      marginVertical:15,
      overflow:'hidden'
    },
    sectionContainer: {
      marginTop: 32,
      padding:22,
      paddingHorizontal: 24,
    },
    sectionTitle: {
      padding: 15,
      color: '#f00',
      fontSize: 24,
      fontWeight: '600',
    },
    sectionDescription: {
      marginTop: 8,
      fontSize: 18,
      fontWeight: '400',
    },
    highlight: {
      fontWeight: '700',
    },
    tinyLogo: {
      width: 50,
      height: 50,
      borderRadius:50
    },
    logo: {
      width: 66,
      height: 58,
    },
    image: {
      flex: 1,
      resizeMode: "cover",
      justifyContent: "center"
    },
    text: {
      color: "white",
      fontSize: 42,
      height:40,
      fontWeight: "bold",
      textAlign: "center",
      backgroundColor: "#000000a0"
    },

    /**
     * Home Products
     */
    home_products: {
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        // justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: -15
    },
    product_space: {
        width: '50%',
        paddingHorizontal: 15,
        marginBottom: 15
    },
    home_categories: {
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        // justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: -10 
    },
    category_space: {
        width: '33%',
        paddingHorizontal: 10,
        marginBottom: 15
    },
    product_image: {
      width: '100%',
      resizeMode:'contain',
      borderRadius:15
    },
    wrapper: {
      height:300,

    },
    slide: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      // backgroundColor: 'blue'
    },
    
});