import { apiExecute } from "."

export const add_Wishlist = async(data) => {
    let res = await apiExecute("add_wishlist","POST", data, {auth:true});
    
    return res;
}


export const remove_Wishlist = async(data) => {
    
    let res = await apiExecute("remove_wishlist", "POST", data, {auth: true});
    return res;
}

export const show_Wishlist = async(data) => {
    
    let res = await apiExecute("show_wishlist", "POST", data, {auth: true});
    
    return res;
}