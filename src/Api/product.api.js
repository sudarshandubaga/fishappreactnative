import { apiExecute } from "."

// export const get_product = async () => {
//     let res = await apiExecute("product");
//     return res;
// }

export const get_products = async () => {
    let params = {
        // products_id : 1,
        
    }
    let res = await apiExecute("getpopularproducts", "POST",{ data: params}, {auth: true});
    return res;
}

export const getpopularproducts = async () => {
    let params = {
        is_feature : 1,
        language_id : 1,
    }
    let res = await apiExecute("getproducts", "POST",params, {auth: true});
    // console.log('res',res);
    return res;
}

export const getcategoryToproducts = async (id) => {
    let params = {
        category_id : id,
        language_id : 1,
    }
    let res = await apiExecute("getproducts", "POST",params, {auth: true});
    // console.log('res',res);
    return res;
}

export const getproductdetail = async (product_id) => {
    let params = {
        product_id : product_id,
        language_id : 1
    }
    let res = await apiExecute("getproductdetail", "POST",params, {auth: true});
    return res;
}
