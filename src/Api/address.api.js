import { apiExecute } from "."

export const cities = async(data) => {
    let res = await apiExecute("cities","GET", data, {auth:true});
    
    return res;
}


export const add_address = async(data) => {
    
    let res = await apiExecute("addaddress", "POST", data, {auth: true});
    return res;
}

export const addresses = async(data) => {
    let res = await apiExecute("getalladdress","POST", data, {auth:true});
    
    return res;
}

export const remove_address = async(data) => {
    let res = await apiExecute("deleteshippingaddress" ,"POST", data, {auth:true});
    return res;
}

export const update_address = async(data) => {
    // console.log('data',data);
    let res = await apiExecute("updateaddress" ,"POST", data, {auth:true});
    return res;
}