import { apiExecute } from "."


export const get_main_categories = async(data) => {
    let res = await apiExecute("getMainCategories?language_id=1", "POST", data, {auth: true});
    return res;
}

export const get_categories = async(id) => {
    let params = {
        category_id : id,
    }
    let res = await apiExecute("getcategories", "POST", params, {auth: true});
    return res;
}