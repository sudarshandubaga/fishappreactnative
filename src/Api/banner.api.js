import { apiExecute } from "."

export const getbanners = async(data) => {
    let res = await apiExecute("getbanners", "GET", data, {auth: true});
    // console.log('res',res.data);
    return res;
}