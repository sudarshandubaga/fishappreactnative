import AsyncStorage from "@react-native-async-storage/async-storage";
import axios from "axios";
import { API_BASE_URL } from "../configs/constants.config";
import DeviceInfo from 'react-native-device-info';
import md5 from "react-native-md5";

export const apiExecute = async (url, method = 'GET', data = null, params = {}) => {
    // let devId = uuid
    let deviceId = DeviceInfo.getDeviceId();
    const d = new Date();

    let consumerKey = "8372e1d916062386052577213f",
        consumerSecret = "a2eae9a51606238605f480c868";

    let headers = {
        // "Content-Type": "application/json",
        "Accept": "application/json",
        'consumer-key': md5.hex_md5(consumerKey).toString(),
        'consumer-secret': md5.hex_md5(consumerSecret).toString(),
        "consumer-nonce": d.getMilliseconds().toString() +
            d.getTime().toString() +
            '-' +
            Math.floor(Math.random() * 999) +
            1,
        "consumer-device-id": deviceId,
        "consumer-ip": "192.168.129.142" + '.' + d.getMilliseconds().toString()
    }



    if(params.auth) {
        let token = await AsyncStorage. getItem('@token');
        headers["Authorization"] = "Bearer " + token
    }
    if(params.files) {
        headers["Content-Type"] = "multipart/form-data";
    }

    let instance = axios.create({
        baseURL: API_BASE_URL,
        timeout: 2000,
        headers
    });

    return instance.request({
        url,
        method,
        data
    })
        .then(res => {
            // console.log('main api res: ', res);
            return {
                status: true,
                data: res.data
            }
        })
        .catch(err => {
            console.log('API Error: ', url, err);
            return {
                status: false,
                error: err.response.data
            }
        });
}