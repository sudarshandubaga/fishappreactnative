import { apiExecute } from ".";

export const sendOtp = async (mobile) => {
    data = {
        mobile:mobile
    }
    let res = await apiExecute("sendOtp", "POST", data);
    return res;
}

export const login = async (data) => {
    
    let res = await apiExecute("auth/login", "POST", data);
    return res;
}

export const verifyOtp = async (otp, mobile) => {
    data = {
        mobile : mobile,
        otp : otp
    }
    let res = await apiExecute("verifyOtp", "POST", data);
    return res;
}
export const register = async (user, mobile) => {
    data = {
        mobile : mobile,
        username : user.username,
        email : user.email,
        referral_code: user.referral_code
    }
    console.log('data',data);
    let res = await apiExecute("register", "POST", data);
    return res;
}
export const edit_profile = async (data) => {
    let res = await apiExecute("edit_profile", "POST", data,{auth:true, files: true});
    console.log('res',res);
    return res;
}

export const userDetail = async (data) => {
    let res = await apiExecute("user_detail", "GET", data,{auth:true});
    
    return res;
}
export const logout = async (data) => {
    
    let res = await apiExecute("logout", "GET", data);
    return res;
}