import { apiExecute } from "."



export const get_Couponlist = async(data) => {
    
    let res = await apiExecute("getcouponlist", "POST", data, {auth: true});
    
    return res;
}