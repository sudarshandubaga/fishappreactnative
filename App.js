/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { Component } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import {
  StyleSheet,
} from 'react-native';
import LoginScreen from './src/screens/LoginScreen/LoginScreen';
import OtpverifyScreen from './src/screens/LoginScreen/OtpverifyScreen';
import RegistrationScreen from './src/screens/LoginScreen/RegistrationScreen';
import HomeScreen from './src/screens/HomeScreen/HomeScreen';
import CategoryScreen from './src/screens/CategoryScreen';
import ProductsScreen from './src/screens/ProductScreen/ProductsScreen';
import ProductScreen from './src/screens/ProductScreen/ProductScreen';
import CartScreen from './src/screens/CartScreen/CartScreen';
import OrderScreen from './src/screens/OrderScreen/OrderScreen';
import WishlistScreen from './src/screens/WishlistScreen/WishlistScreen';
import SiderDrawer from './src/screens/SideDrawer';
import ProfileScreen from './src/screens/ProfileScreen/ProfileScreen';
import AccountScreen from './src/screens/ProfileScreen/AccountScreen';
import AddressScreen from './src/screens/ProfileScreen/AddressScreen';
import AddaddressScreen from './src/screens/ProfileScreen/AddaddressScreen';
import SearchproductScreen from './src/screens/SearchproductScreen';
import CouponListScreen from './src/screens/CartScreen/CouponListScreen';
import AsyncStorage from '@react-native-async-storage/async-storage';


const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

// export default NavigationDrawerStructure = (props)=> {
//   //Structure for the navigatin Drawer
//   const toggleDrawer = () => {
//     //Props to open/close the drawer
//     props.navigationProps.toggleDrawer();
//   };


export default class App extends Component{
  state = {
    user: null,
    token: null,
    loading: false
  }
  getUserdetail = async () => {
    let user = await AsyncStorage.getItem('@token');
    this.setState({user_session: user});
    console.log('fdfnkjgfklgflgdl',this.state.user_session);
      
  }

  async componentDidMount() {
    let user = await AsyncStorage.getItem('@user');
    let token = await AsyncStorage.getItem('@token');

    if (user && token) {
      user = JSON.parse(user);
      this.setState({
        user: user,
        token: token
      });
    }

    setTimeout(() => {
      this.setState({ loading: true })
    }, 5000);

  }
  
  render(){
    // console.log('token', this.state.token);
    let token = AsyncStorage.getItem('@token');
    return(
      <>
      <NavigationContainer>
        <Stack.Navigator initialRouteName={ token ? "Home" : "Login"}>
           
            <Stack.Screen name="Login" component={LoginScreen} options={{ headerShown:false }} />
              
            <Stack.Screen name="Home" component={HomeScreen} options={{ headerShown: false }} />
           
            
          

          
          <Stack.Screen name="Category" component={CategoryScreen} options={{ headerShown: false }} />
          <Stack.Screen name="Products" component={ProductsScreen} options={{ headerShown: false }} />      
          <Stack.Screen name="Product" component={ProductScreen} options={{ headerShown: false }} />
          <Stack.Screen name="Cart" component={CartScreen} options={{ headerShown: false }} />
          <Stack.Screen name="Order" component={OrderScreen} options={{ headerShown: false }} />
          <Stack.Screen name="Wishlist" component={WishlistScreen} options={{ headerShown: false }} />
          <Stack.Screen name="Profile" component={ProfileScreen} options={{ headerShown:false }} />
          <Stack.Screen name="Account" component={AccountScreen} options={{ headerShown:false }} />
          <Stack.Screen name="Address" component={AddressScreen} options={{ headerShown:false }} />
          
          <Stack.Screen name="Otpverify" component={OtpverifyScreen} options= {{ headerShown:false }} />
          <Stack.Screen name="Registration" component={RegistrationScreen} options={{ headerShown:false }} />
          <Stack.Screen name="Add Address" component={AddaddressScreen} options={{ headerShown: false }} />
          <Stack.Screen name="Search Product" component={SearchproductScreen} options={{  headerShown: false}} />
          <Stack.Screen name="Coupon List" component={CouponListScreen} options={{ headerShown: false }} />
        </Stack.Navigator>
        {/* <Drawer.Navigator>
          <Drawer.Screen name="Home" component={HomeScreen} />
        </Drawer.Navigator> */}
      </NavigationContainer>
    </>
    );
  }
}